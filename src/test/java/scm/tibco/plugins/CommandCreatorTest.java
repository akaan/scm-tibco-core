/**
 * 
 */
package scm.tibco.plugins;

import static org.junit.Assert.assertEquals;

import java.net.MalformedURLException;

import org.junit.Before;
import org.junit.Test;

/**
 * @author akaan
 * 
 */
public class CommandCreatorTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		CommandCreator.WINDOWS_FLAG = true;
	}

	@Test
	public void testGetPath() throws MalformedURLException {
		CommandCreator.WINDOWS_FLAG = true;
		assertEquals("/Test/tra/5.8/bin",
				CommandCreator.getPath("C:\\Test", "5.8"));
		assertEquals("/opt/tibco/tra/5.8/bin",
				CommandCreator.getPath("/opt/tibco", "5.8"));
	}


	@Test
	public void testGetStudioPath() throws MalformedURLException {
		CommandCreator.WINDOWS_FLAG = true;
		assertEquals("/Test/be/5.8/studio/bin",
				CommandCreator.getStudioPath("C:\\Test", "5.8"));
		assertEquals("/opt/tibco/be/5.8/studio/bin",
				CommandCreator.getStudioPath("/opt/tibco", "5.8"));
	}
	
}
