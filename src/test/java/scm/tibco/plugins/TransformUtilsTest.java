package scm.tibco.plugins;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.beans.IntrospectionException;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.JAXBElement;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import scm.core.AbstractRemoteInvocationMojo;
import scm.core.ScmWorkingRepository;

import com.fedex.scm.tibco.project.DeploymentPlan;
import com.fedex.scm.tibco.project.GlobalDefinitionSet;
import com.fedex.scm.tibco.project.Node;
import com.fedex.scm.tibco.project.ObjectFactory;
import com.fedex.scm.tibco.project.Project;
import com.tibco.xmlns.applicationmanagement.ApplicationType;
import com.tibco.xmlns.applicationmanagement.Binding;
import com.tibco.xmlns.applicationmanagement.ServiceType;

public class TransformUtilsTest {

	private final int PROP_SIZE = 34;
	
	private static class TransformsTestMojo extends AbstractTibcoMojo {

		@Override
		public void execute() throws MojoExecutionException,
				MojoFailureException {
			// TODO Auto-generated method stub

		}

	}

	private ApplicationType application = null;
	final ObjectFactory f = new ObjectFactory();
	private TransformsTestMojo mojo = null;

	private Project project = null;

	private void loadResource1TwoNodes() {
		project = (Project) mojo.unmarshaller.unmarshal(new StreamSource(
				"src/test/resources/resource-1.xml"));
	}

	private void loadPlan0GlobalDefsOnly() {
		project = (Project) mojo.unmarshaller.unmarshal(new StreamSource(
				"src/test/resources/plan-0-globaldef-only.xml"));
	}

	private void loadPlan1GlobalDefsOnlyWithTwoNodes() {
		project = (Project) mojo.unmarshaller.unmarshal(new StreamSource(
				"src/test/resources/plan-1-globaldef-only-two-nodes.xml"));
	}

	private Project loadPlan2PickupOfferWithThreeNodes() {
		project = (Project) mojo.unmarshaller.unmarshal(new StreamSource(
				"src/test/resources/plan-2-pickupoff-three-nodes.xml"));
		return project;
	}

	private Project loadPlan3PickupOfferWith2Plans2Nodes() {
		project = (Project) mojo.unmarshaller.unmarshal(new StreamSource(
				"src/test/resources/plan-3-pickupoff-2-plans-2-nodes.xml"));
		return project;
	}

	private ApplicationType loadAppl2PickupOfferWithThreeNodes() {
		@SuppressWarnings("unchecked")
		JAXBElement<ApplicationType> element = (JAXBElement<ApplicationType>) mojo.unmarshaller
				.unmarshal(new StreamSource(
						"src/test/resources/appl-2-pickupoff-three-nodes.xml"));
		application = element.getValue();
		return application;
	}

	@Before
	public void setUp() throws Exception {
		mojo = new TransformsTestMojo();
		mojo.initialize();
		project = (Project) mojo.unmarshaller.unmarshal(new StreamSource(
				"src/test/resources/resource-0.xml"));
		@SuppressWarnings("unchecked")
		JAXBElement<ApplicationType> element = (JAXBElement<ApplicationType>) mojo.unmarshaller
				.unmarshal(new StreamSource(
						"src/test/resources/resource-filtering-application.xml"));
		application = element.getValue();
	}

	@Test
	public void testApplyGlobalDefinitions() {
		Node aNode = f.createNode();
		assertNotNull(aNode);
		Properties properties = new Properties();
		try {
			aNode = TransformUtils.applyGlobalDefinitions(null, aNode,
					properties);
			fail("should have failed on the global id.");
		} catch (Exception e) {

		}
		aNode.setGlobalId(f.createGlobalDefinitionSet());
		try {
			aNode = TransformUtils.applyGlobalDefinitions(null, aNode,
					properties);
			fail("should have failed on missing enginebasename on node and definition.");
		} catch (NullPointerException npe) {
		}
		assertNotNull(aNode);
		assertEquals(aNode.getEngineName(), null);
	}

	@Test
	public void testPlan0ApplyGlobalDefinitionsToPredefinedNode()
			throws IntrospectionException {
		loadPlan0GlobalDefsOnly();
		DeploymentPlan plan = project.getPlans().getPlan().get(0);
		assertEquals(2, plan.getNumberOfNodes());
		TransformUtils.resizeNodes(plan);
		Properties p = TransformUtils.extractGlobals(plan);
		p.setProperty("nodeNumber", "01");
		plan = TransformUtils.resizeNodes(plan);
		TransformUtils.applyGlobalDefinitions(plan.getNodes(), p);
		assertPlan0(plan, p);
	}

	@Test
	public void testPlan0ApplyGlobalDefinitionsToPredefinedNodeToBinding()
			throws IntrospectionException {
		loadPlan0GlobalDefsOnly();
		DeploymentPlan plan = project.getPlans().getPlan().get(0);
		assertEquals(2, plan.getNumberOfNodes());
		TransformUtils.resizeNodes(plan);
		Properties p = TransformUtils.extractGlobals(plan);
		p.setProperty("nodeNumber", "01");
		plan = TransformUtils.resizeNodes(plan);
		TransformUtils.applyGlobalDefinitions(plan.getNodes(), p);
		assertPlan0(plan, p);
		List<Binding> bindings = new ArrayList<Binding>();
		for (Iterator<Node> iterator = plan.getNodes().getNode().iterator(); iterator
				.hasNext();) {
			Node type = (Node) iterator.next();
			bindings.add(TransformUtils.nodeToBinding(plan, type, p));
		}
		assertPlan0Binding(plan, bindings);
	}

	private void assertPlan0(DeploymentPlan plan, Properties p) {
		assertEquals(2, plan.getNodes().getNode().size());
		assertEquals(PROP_SIZE, p.size());
		int i = 1;
		for (Iterator<Node> iterator = plan.getNodes().getNode().iterator(); iterator
				.hasNext();) {
			Node aNode = iterator.next();
			assertEquals("engine+" + i, "LoggerService_0" + i,
					aNode.getEngineName());
			assertEquals("machine+" + i, "drh0028%s.ground.fedex.com",
					aNode.getMachineName());
			i++;
		}
	}

	private void assertPlan0Binding(DeploymentPlan plan, List<Binding> bindings) {
		assertEquals(plan.getNodes().getNode().size(), bindings.size());
		System.out.println(ReflectionToStringBuilder.toString(bindings));
		int i = 0;
		for (Iterator<Binding> iterator = bindings.iterator(); iterator
				.hasNext();) {
			Binding binding = (Binding) iterator.next();
			assertEquals("binding" + i,
					String.format("drh0028%d.ground.fedex.com", i + 1),
					binding.getMachine());
			assertEquals("binding" + i,
					String.format("LoggerService_%02d", i + 1),
					binding.getName());
			assertEquals(new BigInteger("10"), binding.getSetting()
					.getThreadCount());
			assertEquals(new BigInteger("128"), binding.getSetting().getJava()
					.getInitHeapSize());
			assertEquals(new BigInteger("512"), binding.getSetting().getJava()
					.getMaxHeapSize());
			assertEquals(null, binding.getSetting().getJava()
					.getPrepandClassPath());
			i++;
		}
	}

	/* ----------------------------------------------------------- */
	/*
	 * PLAN 1 TEST CASE /*
	 * -----------------------------------------------------------
	 */

	@Test
	public void testPlan1ApplyGlobalDefinitionsToPredefinedNode()
			throws IntrospectionException {
		loadPlan1GlobalDefsOnlyWithTwoNodes();
		DeploymentPlan plan = project.getPlans().getPlan().get(0);
		assertEquals(2, plan.getNumberOfNodes());
		TransformUtils.resizeNodes(plan);
		Properties p = TransformUtils.extractGlobals(plan);
		p.setProperty("nodeNumber", "01");
		plan = TransformUtils.resizeNodes(plan);
		TransformUtils.applyGlobalDefinitions(plan.getNodes(), p);
		assertPlan1(plan, p);
	}

	@Test
	public void testPlan1ApplyGlobalDefinitionsToPredefinedNodeToBinding()
			throws IntrospectionException {
		loadPlan1GlobalDefsOnlyWithTwoNodes();
		DeploymentPlan plan = project.getPlans().getPlan().get(0);
		assertEquals(2, plan.getNumberOfNodes());
		TransformUtils.resizeNodes(plan);
		Properties p = TransformUtils.extractGlobals(plan);
		p.setProperty("nodeNumber", "01");
		plan = TransformUtils.resizeNodes(plan);
		TransformUtils.applyGlobalDefinitions(plan.getNodes(), p);
		assertPlan1(plan, p);
		List<Binding> bindings = new ArrayList<Binding>();
		for (Iterator<Node> iterator = plan.getNodes().getNode().iterator(); iterator
				.hasNext();) {
			Node type = (Node) iterator.next();
			bindings.add(TransformUtils.nodeToBinding(plan, type, p));
		}
		assertPlan1Binding(plan, bindings);
	}

	private void assertPlan1(DeploymentPlan plan, Properties p) {
		assertEquals(2, plan.getNodes().getNode().size());
		System.err.println(p);
		assertEquals(PROP_SIZE, p.size());
		assertEquals(p.getProperty("id"), "plan-1");
		int i = 0;
		for (Iterator<Node> iterator = plan.getNodes().getNode().iterator(); iterator
				.hasNext();) {
			Node aNode = iterator.next();
			if (i == 0) {
				assertEquals("engine+" + i, "EngineName_01",
						aNode.getEngineName());
				assertEquals("machine+" + i, "urh00110.ground.fedex.com",
						aNode.getMachineName());
			} else {
				assertEquals("engine+" + i, "LoggerService_0" + (i + 1),
						aNode.getEngineName());
				assertEquals("machine+" + i, "drh0028%s.ground.fedex.com",
						aNode.getMachineName());
			}
			i++;
		}
	}

	private void assertPlan1Binding(DeploymentPlan plan, List<Binding> bindings) {
		assertEquals(plan.getNodes().getNode().size(), bindings.size());
		System.out.println(ReflectionToStringBuilder.toString(bindings));
		int i = 0;
		for (Iterator<Binding> iterator = bindings.iterator(); iterator
				.hasNext();) {
			Binding binding = (Binding) iterator.next();
			if (i == 0) {
				assertEquals("binding" + i, "urh00110.ground.fedex.com",
						binding.getMachine());
				assertEquals("binding" + i, "EngineName_01", binding.getName());
			} else {
				assertEquals("binding" + i,
						String.format("drh0028%d.ground.fedex.com", i + 1),
						binding.getMachine());
				assertEquals("binding" + i,
						String.format("LoggerService_%02d", i + 1),
						binding.getName());
			}
			assertEquals(new BigInteger("10"), binding.getSetting()
					.getThreadCount());
			assertEquals(new BigInteger("128"), binding.getSetting().getJava()
					.getInitHeapSize());
			assertEquals(new BigInteger("512"), binding.getSetting().getJava()
					.getMaxHeapSize());
			assertEquals(null, binding.getSetting().getJava()
					.getPrepandClassPath());
			i++;
		}
	}

	/* ----------------------------------------------------------- */
	/*
	 * PLAN 2 TEST CASE /*
	 * -----------------------------------------------------------
	 */

	@Test
	public void testPlan2ApplyGlobalDefinitionsToPickupOffer()
			throws IntrospectionException {
		project = loadPlan2PickupOfferWithThreeNodes();
		DeploymentPlan plan = project.getPlans().getPlan().get(0);
		assertEquals(3, plan.getNumberOfNodes());
		TransformUtils.resizeNodes(plan);
		Properties p = TransformUtils.extractGlobals(plan);
		plan = TransformUtils.resizeNodes(plan);
		TransformUtils.applyGlobalDefinitions(plan.getNodes(), p);
		assertPlan2(3, plan, p);
		List<Binding> bindings = new ArrayList<Binding>();
		for (Iterator<Node> iterator = plan.getNodes().getNode().iterator(); iterator
				.hasNext();) {
			Node node = (Node) iterator.next();
			bindings.add(TransformUtils.nodeToBinding(plan, node, p));
		}
		assertPlan2Binding(plan, bindings);
	}

	@Test
	public void testPlan2Application() throws IntrospectionException {
		project = loadPlan2PickupOfferWithThreeNodes();
		application = loadAppl2PickupOfferWithThreeNodes();
		DeploymentPlan plan = project.getPlans().getPlan().get(0);
		assertEquals(3, plan.getNumberOfNodes());
		TransformUtils.resizeNodes(plan);
		Properties p = TransformUtils.extractGlobals(plan);
		plan = TransformUtils.resizeNodes(plan);
		TransformUtils.applyGlobalDefinitions(plan.getNodes(), p);
		assertPlan2(3, plan, p);
		TransformUtils.interpolateProjectWithApplication(plan, application, p);
		/* assert application */
		JAXBElement<? extends ServiceType> service_type = application
				.getServices().getBaseService().get(0);
		assertNotNull(service_type);
		ServiceType service = service_type.getValue();
		assertNotNull(service);
		assertEquals(3, service.getBindings().getBinding().size());
		assertPlan2Binding(plan, service.getBindings().getBinding());

		mojo.marshaller.marshal(application, new StreamResult(
				"/tmp/streamresult.xml"));
	}

	private void assertPlan2(int size, DeploymentPlan plan, Properties p) {
		assertPlan2(size, PROP_SIZE, "plan-1", plan, p);
	}

	/**
	 * 
	 * @param planSize
	 * @param propertySize
	 * @param id
	 * @param plan
	 * @param p
	 */
	private void assertPlan2(int planSize, int propertySize, String id,
			DeploymentPlan plan, Properties p) {
		assertEquals(planSize, plan.getNodes().getNode().size());
		assertEquals(propertySize, p.size());
		assertEquals(p.getProperty("id"), id);
		int i = 0;
		for (Iterator<Node> iterator = plan.getNodes().getNode().iterator(); iterator
				.hasNext();) {
			Node aNode = iterator.next();
			assertEquals(id + "+machine+" + i, "drh0028%d.ground.fedex.com",
					aNode.getMachineName());
			assertEquals(id + "engine+" + i, "PickupProject_%02d",
					aNode.getEngineName());
			i++;
		}
	}

	private void assertPlan2Binding(DeploymentPlan plan, List<Binding> bindings) {
		assertEquals(plan.getNodes().getNode().size(), bindings.size());
		System.out.println(ReflectionToStringBuilder.toString(bindings));
		int i = 0;
		for (Iterator<Binding> iterator = bindings.iterator(); iterator
				.hasNext();) {
			Binding binding = (Binding) iterator.next();
			assertEquals("binding" + i,
					String.format("drh0028%d.ground.fedex.com", i),
					binding.getMachine());
			assertEquals("binding" + i,
					String.format("PickupProject_%02d", i + 1),
					binding.getName());
			assertEquals(new BigInteger("10"), binding.getSetting()
					.getThreadCount());
			assertEquals(new BigInteger("128"), binding.getSetting().getJava()
					.getInitHeapSize());
			assertEquals(new BigInteger("512"), binding.getSetting().getJava()
					.getMaxHeapSize());
			assertEquals(
					"/opt/fedex/tibco/stat/external-libraries-bpms/current/bw/lib",
					binding.getSetting().getJava().getPrepandClassPath());
			i++;
		}
	}

	private void assertPlan3Binding(DeploymentPlan plan, List<Binding> bindings) {
		assertEquals(plan.getNodes().getNode().size(), bindings.size());
		System.out.println(ReflectionToStringBuilder.toString(bindings));
		int i = 0;
		for (Iterator<Binding> iterator = bindings.iterator(); iterator
				.hasNext();) {
			Binding binding = (Binding) iterator.next();
			assertEquals("binding" + i,
					String.format("drh0028%d.ground.fedex.com", i + 7),
					binding.getMachine());
			assertEquals("binding" + i,
					String.format("PickupProject_%02d", i + 7),
					binding.getName());
			assertEquals(new BigInteger("12"), binding.getSetting()
					.getThreadCount());
			assertEquals(new BigInteger("256"), binding.getSetting().getJava()
					.getInitHeapSize());
			assertEquals(new BigInteger("512"), binding.getSetting().getJava()
					.getMaxHeapSize());
			assertEquals(
					"/opt/fedex/tibco/stat/external-libraries-bpms/current/bw/lib",
					binding.getSetting().getJava().getPrepandClassPath());
			i++;
		}
	}

	@Test
	public void testPlan3PickupOfferWith2Plans2Nodes()
			throws IntrospectionException {
		project = loadPlan3PickupOfferWith2Plans2Nodes();
		assertEquals(2, project.getPlans().getPlan().size());
		DeploymentPlan plan = project.getPlans().getPlan().get(0);
		assertPlan3FirstPlan(plan, "id-1");
		assertPlan3SecondPlan(project.getPlans().getPlan().get(1), "id-2");
	}

	private void assertPlan3SecondPlan(DeploymentPlan plan, String planId)
			throws IntrospectionException {
		assertEquals(2, plan.getNumberOfNodes());
		assertEquals(planId, plan.getId());

		Properties p = TransformUtils.extractGlobals(plan);
		
		assertEquals("componentName", "PickupCoordinator", p.getProperty("componentName"));

		TransformUtils.applyGlobalDefinitions(plan.getNodes(), p);
		assertPlan3Node2(2, PROP_SIZE+1, planId, plan, p);

		Node type = plan.getNodes().getNode().get(0);
		assertEquals(type.getId(), "node3");
		
		Binding b = TransformUtils.nodeToBinding(plan, type, p);
		assertEquals("vrh00428.ute.fedex.com",b.getMachine());
		assertEquals("PickupCoordinator_08", b.getName());
	}

	private void assertPlan3Node2(int planSize, int propertySize, String planId,
			DeploymentPlan plan, Properties p) {
		assertEquals(planSize, plan.getNodes().getNode().size());
		assertEquals(propertySize, p.size());
		assertEquals(p.getProperty("id"), planId);
		int i = 0;
		for (Iterator<Node> iterator = plan.getNodes().getNode().iterator(); iterator
				.hasNext();) {
			Node aNode = iterator.next();
			assertEquals(planId + "+machine+" + i, "vrh0042%d.ute.fedex.com",
					aNode.getMachineName());
			assertEquals(planId+ "+engine+" + i, "PickupCoordinator_%02d",
					aNode.getEngineName());
			i++;
		}

	}

	private void assertPlan3FirstPlan(DeploymentPlan plan, String planId)
			throws IntrospectionException {
		assertEquals(2, plan.getNumberOfNodes());
		assertEquals(planId, plan.getId());

		Properties p = TransformUtils.extractGlobals(plan);

		TransformUtils.applyGlobalDefinitions(plan.getNodes(), p);
		assertPlan2(2, PROP_SIZE+1, planId, plan, p);

		Node type = plan.getNodes().getNode().get(0);
		Binding b = TransformUtils.nodeToBinding(plan, type, p);
		assertEquals(b.getMachine(), "drh00287.ground.fedex.com");
		assertEquals(b.getName(), "PickupProject_07");

		assertPlan3(plan, planId, p);
		TransformUtils.interpolateProjectWithApplication(plan, application, p);

		/* assert application */
		JAXBElement<? extends ServiceType> service_type = application
				.getServices().getBaseService().get(0);
		assertNotNull(service_type);
		ServiceType service = service_type.getValue();
		assertNotNull(service);
		assertEquals(2, service.getBindings().getBinding().size());
		assertPlan3Binding(plan, service.getBindings().getBinding());

		mojo.marshaller.marshal(application, new StreamResult(
				"/tmp/streamresult.xml"));
	}

	private void assertPlan3(DeploymentPlan plan, Properties p) {
		assertPlan3(plan, "plan-1", p);
	}

	private void assertPlan3(DeploymentPlan plan, String id, Properties p) {
		assertEquals(2, plan.getNodes().getNode().size());
		assertEquals(PROP_SIZE+1, p.size());
		assertEquals(p.getProperty("id"), id);
		assertEquals(p.getProperty("componentName"), "PickupProject");
		int i = 0;
		for (Iterator<Node> iterator = plan.getNodes().getNode().iterator(); iterator
				.hasNext();) {
			Node aNode = iterator.next();
			assertEquals("engine+" + i, "PickupProject_%02d",
					aNode.getEngineName());
			assertEquals("machine+" + i, "drh0028%d.ground.fedex.com",
					aNode.getMachineName());
			i++;
		}
	}

	/*
	 * ----------------------------------------------------------- RESOURCE 0
	 * TEST CASE -----------------------------------------------------------
	 */

	@Test
	public void testResource0NodeToBindingAndInterpolatingTwoNodes()
			throws IntrospectionException {
		DeploymentPlan plan = project.getPlans().getPlan().get(0);
		assertEquals(2, plan.getNumberOfNodes());
		assertEquals(2, plan.getNodes().getNode().size());
		Properties p = TransformUtils.extractGlobals(plan);

		// Expand
		plan.setNumberOfNodes(3);
		plan = TransformUtils.resizeNodes(plan);
		assertEquals(3, plan.getNodes().getNode().size());
		assertEquals("node1", plan.getNodes().getNode().get(0).getId());
		assertEquals("node2", plan.getNodes().getNode().get(1).getId());

		// applyGlobalDefinitions.
		TransformUtils.applyGlobalDefinitions(plan.getNodes(), p);

		assertResource0Plan(plan, p);

		TransformUtils.interpolateProjectWithApplication(plan, application, p);
		List<Binding> bi = application.getServices().getBaseService().get(0)
				.getValue().getBindings().getBinding();
		assertEquals(3, bi.size());

		assertResource0Binding(plan, bi);

		mojo.marshaller.marshal(application, new StreamResult(
				"/tmp/streamresult.xml"));
	}

	private void assertResource0Plan(DeploymentPlan plan, Properties p) {
		assertEquals(3, plan.getNodes().getNode().size());
		assertEquals(PROP_SIZE, p.size());
		assertEquals(p.getProperty("id"), "plan-1");

		int i = 0;
		for (Iterator<Node> iterator = plan.getNodes().getNode().iterator(); iterator
				.hasNext();) {
			Node node = iterator.next();
			if (i == 0) {
				assertEquals("nodename" + i, "1",
						String.valueOf(node.getNodeNumber()));
				assertEquals("nodemachinename" + i,
						"drh00280.ground.fedex.com", node.getMachineName());
				assertEquals("nodenginename" + i, "LoggerService_01",
						node.getEngineName());
			} else if (i == 1) {
				assertEquals("nodename" + i, "2",
						String.valueOf(node.getNodeNumber()));
				assertEquals("nodemachinename" + i, "localhost",
						node.getMachineName());
				assertEquals("nodenginename" + i, "LoggerService_02",
						node.getEngineName());
			} else if (i == 2) {
				assertEquals("nodename" + i, "3",
						String.valueOf(node.getNodeNumber()));
				assertEquals("nodemachinename" + i,
						"ppbeggl%02d.ground.fede.com", node.getMachineName());
				assertEquals("nodenginename" + i, "LoggerService_03",
						node.getEngineName());
			} else {
				fail("should not have been reached");
			}
			i++;
		}
	}

	private void assertResource0Binding(DeploymentPlan plan,
			List<Binding> bindings) {
		assertEquals(plan.getNodes().getNode().size(), bindings.size());
		System.out.println(ReflectionToStringBuilder.toString(bindings));
		int i = 0;
		String[] value = null;
		for (Iterator<Binding> iterator = bindings.iterator(); iterator
				.hasNext();) {
			Binding binding = (Binding) iterator.next();
			if (i == 0) {
				value = new String[] { "10", "128", "512", "256" };
				assertEquals("binding" + i, "drh00280.ground.fedex.com",
						binding.getMachine());

			} else if (i == 1) {
				value = new String[] { "10", "256", "256", "128" };
				assertEquals("binding" + i, "localhost", binding.getMachine());
			} else {
				value = new String[] { "5", "128", "256", "128" };
				assertEquals("binding" + i,
						String.format("ppbeggl%02d.ground.fede.com", i + 1),
						binding.getMachine());

			}
			assertEquals("binding" + i,
					String.format("LoggerService_%02d", i + 1),
					binding.getName());
			assertEquals("binding" + i, new BigInteger(value[0]), binding
					.getSetting().getThreadCount());

			assertEquals("binding" + i, new BigInteger(value[1]), binding
					.getSetting().getJava().getInitHeapSize());

			assertEquals("binding" + i, new BigInteger(value[2]), binding
					.getSetting().getJava().getMaxHeapSize());

			assertEquals("binding" + i, new BigInteger(value[3]), binding
					.getSetting().getJava().getThreadStackSize());

			assertEquals(
					"/opt/fedex/tibco/stat/external-libraries-bpms/current/bw/lib",
					binding.getSetting().getJava().getPrepandClassPath());
			i++;
		}
	}

	/* ----------------------------------------------------------- */

	@Test
	public void testCreateRemoteWorkDirectoryRef() throws IOException {
		AbstractRemoteInvocationMojo facade = new AbstractRemoteInvocationMojo() {

			@Override
			public void execute() throws MojoExecutionException,
					MojoFailureException {
				// TODO Auto-generated method stub

			}
		};
		ScmWorkingRepository remote = new ScmWorkingRepository(facade,
				"ppawada01.prod.fedex.com:/opt/fedex/wad/stat/1.0/");

		File remoteDeploymentConfigFile = TransformUtils
				.createRemoteWorkDirectoryAndTempXmlFile(remote, false);
		assertNotNull(remoteDeploymentConfigFile);
		assertEquals("/opt/fedex/wad/stat/1.0", remoteDeploymentConfigFile
				.getParentFile().getPath());

		remote = new ScmWorkingRepository(facade,
				"scpexe://drh00280.ground.fedex.com:/var/tmp/");

		remoteDeploymentConfigFile = TransformUtils
				.createRemoteWorkDirectoryAndTempXmlFile(remote, false);
		assertNotNull(remoteDeploymentConfigFile);
		assertEquals("/var/tmp", remoteDeploymentConfigFile.getParentFile()
				.getPath());
	}

	// @Test
	public void testEffectiveDistributionPlan() throws IntrospectionException {
		DeploymentPlan plan = project.getPlans().getPlan().get(0);
		assertEquals(2, plan.getNumberOfNodes());

		Properties p = TransformUtils.extractGlobals(plan);
		assertEquals(2, plan.getNodes().getNode().size());

		plan = TransformUtils.resizeNodes(plan);

		Node aNode = plan.getNodes().getNode().get(0);
		TransformUtils.applyGlobalDefinitions(plan.getNodes(), aNode, p);
		ServiceType service = application.getServices().getBaseService().get(0)
				.getValue();

		assertEquals(1, application.getServices().getBaseService().size());
		assertEquals(1, service.getBindings().getBinding().size());

		TransformUtils.interpolateProjectWithApplication(plan, application, p);

		assertEquals(1, application.getServices().getBaseService().size());
		assertEquals(2, service.getBindings().getBinding().size());

		Node secNode = plan.getNodes().getNode().get(1);
		assertEquals("node2", secNode.getId());
		assertEquals("before", "${componentName}_${nodeNumber}",
				secNode.getEngineBaseName());
		assertEquals("before", "localhost", secNode.getMachineBaseName());
		TransformUtils.applyGlobalDefinitions(plan.getNodes(), secNode, p);
		assertEquals("after", "${componentName}_${nodeNumber}",
				secNode.getEngineBaseName());
		assertEquals("after", "localhost", secNode.getMachineBaseName());
		assertEquals("after",
				"/opt/fedex/tibco/stat/external-libraries-bpms/current/bw/lib",
				secNode.getPrependClasspath());

	}

	@Test
	public void testExtractDummyGlobals() throws IntrospectionException {
		DeploymentPlan plan = f.createDeploymentPlan();
		Properties p = TransformUtils.extractGlobals(plan);
		Assert.assertEquals(null, p.get("projectName"));
		Assert.assertEquals("0", p.get("numberOfNodes"));
		plan.setNumberOfNodes(1);
		p = TransformUtils.extractGlobals(plan);
		System.out.println(p);
		Assert.assertEquals("1", p.get("numberOfNodes"));
	}

	@Test
	public void testResource0ExtractFromFileGlobals()
			throws IntrospectionException {
		DeploymentPlan plan = project.getPlans().getPlan().get(0);
		Properties p = TransformUtils.extractGlobals(plan);
		Assert.assertEquals("BW-5.5-PROJECT", p.get("projectName"));
		Assert.assertEquals("2", p.get("numberOfNodes"));
		Assert.assertEquals("tcp:55550", p.get("daemon"));

		Node aNode = plan.getNodes().getNode().get(0);
		Assert.assertEquals("before", null, aNode.getMachineBaseName());
		TransformUtils.applyGlobalDefinitions(plan.getNodes(), aNode, p);
		Assert.assertEquals("after", 1, aNode.getNodeNumber());
		Assert.assertEquals("after", "LoggerService_01", aNode.getEngineName());
		Assert.assertEquals("after", "${componentName}_${nodeNumber}",
				aNode.getEngineBaseName());
		Assert.assertEquals("after", "drh00280.ground.fedex.com",
				aNode.getMachineName());
		assertEquals("after",
				"/opt/fedex/tibco/stat/external-libraries-bpms/current/bw/lib",
				aNode.getPrependClasspath());

		Node secNode = plan.getNodes().getNode().get(1);
		assertEquals("node2", secNode.getId());
		assertEquals("before", null, secNode.getEngineBaseName());
		assertEquals("before", "localhost", secNode.getMachineBaseName());
		TransformUtils.applyGlobalDefinitions(plan.getNodes(), secNode, p);
		assertEquals("after", "${componentName}_${nodeNumber}",
				secNode.getEngineBaseName());
		assertEquals("after", "LoggerService_02", secNode.getEngineName());
		assertEquals("after", "localhost", secNode.getMachineName());
		assertEquals("after",
				"/opt/fedex/tibco/stat/external-libraries-bpms/current/bw/lib",
				secNode.getPrependClasspath());
	}

	@Test
	public void testInterpolationEngineName() {
		GlobalDefinitionSet set = new GlobalDefinitionSet();
		set.setEngineBaseName("globalEngineBaseName");
		assertEquals("engineName", TransformUtils.interpolateEngineName(
				"engineName", null, null, null, 0));
		assertEquals("engineBaseName", TransformUtils.interpolateEngineName(
				null, "engineBaseName", null, null, 0));
		assertEquals("engineBaseName", TransformUtils.interpolateEngineName(
				null, "engineBaseName", set, null, 0));
		assertEquals("engineBaseName_01", TransformUtils.interpolateEngineName(
				null, "engineBaseName_%02d", set, "01", 0));
		assertEquals("engineBaseName_${nodeNumber}",
				TransformUtils.interpolateEngineName(null,
						"engineBaseName_${nodeNumber}", set, "01", 0));
		assertEquals("globalEngineBaseName",
				TransformUtils.interpolateEngineName(null, null, set, null, 0));
		set.setEngineBaseName("global_%s");
		assertEquals("global_0",
				TransformUtils.interpolateEngineName(null, null, set, null, 0));
		assertEquals("global_1",
				TransformUtils.interpolateEngineName(null, null, set, "01", 0));
	}

	@Test
	public void testInterpolationMachineName() {
		GlobalDefinitionSet set = new GlobalDefinitionSet();
		set.setMachineBaseName("globalMachineBaseName");
		assertEquals("machineName", TransformUtils.interpolateMachineName(
				"machineName", null, null, null, 0));
		assertEquals("machineBaseName", TransformUtils.interpolateMachineName(
				null, "machineBaseName", null, null, 0));
		assertEquals("machineBaseName", TransformUtils.interpolateMachineName(
				null, "machineBaseName", set, null, 0));
		assertEquals("machineBaseName_01",
				TransformUtils.interpolateMachineName(null,
						"machineBaseName_%s", set, "01", 0));
		assertEquals("machineBaseName_${nodeNumber}",
				TransformUtils.interpolateMachineName(null,
						"machineBaseName_${nodeNumber}", set, "01", 0));
		assertEquals("globalMachineBaseName",
				TransformUtils.interpolateMachineName(null, null, set, null, 0));
		set.setMachineBaseName("global_%s");
		assertEquals("global_null",
				TransformUtils.interpolateMachineName(null, null, set, null, 0));
		assertEquals("global_01",
				TransformUtils.interpolateMachineName(null, null, set, "01", 0));
	}


	@Test
	public void testMergeGlobals() throws IntrospectionException {
		mojo.initialize();
		assertNotNull(project);
		// mojo.processPlans(project.getPlans().getPlan());

		@SuppressWarnings("rawtypes")
		JAXBElement _project = (JAXBElement) mojo.unmarshaller
				.unmarshal(new StreamSource(
						"src/test/resources/downstream-7772183010200763707.xml"));
		ApplicationType applicationType = (ApplicationType) _project.getValue();
		System.err.println(ReflectionToStringBuilder.toString(applicationType));
		TransformUtils.interpolateProjectWithApplication(project.getPlans()
				.getPlan().get(0), applicationType, new Properties());

	}

	@Test
	public void testMergePlanIntoApplication() throws IntrospectionException {
		DeploymentPlan plan = project.getPlans().getPlan().get(0);
		Properties p = TransformUtils.extractGlobals(plan);
		Node aNode = plan.getNodes().getNode().get(0);
		TransformUtils.applyGlobalDefinitions(plan.getNodes(), aNode, p);
		TransformUtils.interpolateProjectWithApplication(plan, application, p);

	}

	@Test
	public void testNodeToBinding() throws IntrospectionException {
		DeploymentPlan plan = project.getPlans().getPlan().get(0);
		assertEquals(2, plan.getNumberOfNodes());

		Properties p = TransformUtils.extractGlobals(plan);

		// Expand
		plan.setNumberOfNodes(1);
		plan = TransformUtils.resizeNodes(plan);
		assertEquals(1, plan.getNodes().getNode().size());

		Node node = plan.getNodes().getNode().get(0);
		Binding b = TransformUtils.nodeToBinding(plan, node, p);

		assertEquals("b.machinename", "drh00280.ground.fedex.com",
				b.getMachine());
	}

	// @Test
	public void testNodeToBindingAndInterpolatingAllFourNodesBasedOnGlobal()
			throws IntrospectionException {
		project = (Project) mojo.unmarshaller.unmarshal(new StreamSource(
				"src/test/resources/resource-filtering-all-nodes.xml"));
		DeploymentPlan plan = project.getPlans().getPlan().get(0);
		assertEquals(2, plan.getNumberOfNodes());

		Properties p = TransformUtils.extractGlobals(plan);

		// Expand
		plan.setNumberOfNodes(4);
		plan = TransformUtils.resizeNodes(plan);
		assertEquals(4, plan.getNodes().getNode().size());

		Node node = plan.getNodes().getNode().get(0);
		p.setProperty("nodeNumber", "01");

		// applyGlobalDefinitions.
		TransformUtils.applyGlobalDefinitions(plan.getNodes(), node, p);

		Binding b = TransformUtils.nodeToBinding(plan, node, p);
		assertEquals("LoggerService_01", b.getName());
		assertEquals("b.machinename", "drh00281.ground.fedex.com",
				b.getMachine());

		TransformUtils.interpolateProjectWithApplication(plan, application, p);
		List<Binding> bi = application.getServices().getBaseService().get(0)
				.getValue().getBindings().getBinding();

		assertEquals(4, bi.size());
		Binding bx3 = bi.get(3);
		assertEquals("drh00283.ground.fede.com", bx3.getMachine());
		Binding bx0 = bi.get(0);
		assertEquals("drh00280.ground.fedex.com", bx0.getMachine());
		Binding bx1 = bi.get(1);
		assertEquals("drh00281.ground.fede.com", bx1.getMachine());
		Binding bx2 = bi.get(2);
		assertEquals("drh00282.ground.fede.com", bx2.getMachine());

		mojo.marshaller.marshal(application, new StreamResult(
				"/tmp/streamresult.xml"));
	}

	// @Test
	public void testNodeToBindingAndInterpolatingFourNodesBasedOnGlobal()
			throws IntrospectionException {
		project = (Project) mojo.unmarshaller.unmarshal(new StreamSource(
				"src/test/resources/resource-filtering-two-nodes.xml"));
		DeploymentPlan plan = project.getPlans().getPlan().get(0);
		assertEquals(2, plan.getNumberOfNodes());

		Properties p = TransformUtils.extractGlobals(plan);

		// Expand
		plan.setNumberOfNodes(4);
		plan = TransformUtils.resizeNodes(plan);
		assertEquals(4, plan.getNodes().getNode().size());

		Node node = plan.getNodes().getNode().get(0);
		p.setProperty("nodeNumber", "01");
		// applyGlobalDefinitions.
		TransformUtils.applyGlobalDefinitions(plan.getNodes(), node, p);

		Binding b = TransformUtils.nodeToBinding(plan, node, p);

		assertEquals("LoggerService_01", b.getName());

		assertEquals("b.machinename", "drh00280.ground.fedex.com",
				b.getMachine());

		TransformUtils.interpolateProjectWithApplication(plan, application, p);
		List<Binding> bi = application.getServices().getBaseService().get(0)
				.getValue().getBindings().getBinding();
		assertEquals(4, bi.size());
		Binding bx0 = bi.get(0);
		assertEquals("drh00280.ground.fedex.com", bx0.getMachine());
		Binding bx1 = bi.get(1);
		assertEquals("ppbeggl02.ground.fede.com", bx1.getMachine());
		Binding bx2 = bi.get(2);
		assertEquals("ppbeggl03.ground.fede.com", bx2.getMachine());
		Binding bx3 = bi.get(3);
		assertEquals("ppbeggl04.ground.fede.com", bx3.getMachine());

		mojo.marshaller.marshal(application, new StreamResult(
				"/tmp/streamresult.xml"));
	}

	@Test
	public void testResource1NodeToBindingAndInterpolatingOneNode()
			throws IntrospectionException {
		loadResource1TwoNodes();
		DeploymentPlan plan = project.getPlans().getPlan().get(0);
		assertEquals(2, plan.getNumberOfNodes());

		Properties p = TransformUtils.extractGlobals(plan);

		// Expand
		Node node = plan.getNodes().getNode().get(0);
		// p.setProperty("nodeNumber", "00");
		// applyGlobalDefinitions.
		TransformUtils.applyGlobalDefinitions(plan.getNodes(), node, p);

		assertEquals("${componentName}_${nodeNumber}", node.getEngineBaseName());
		assertEquals("drh0028%s.ground.fedex.com", node.getMachineBaseName());
		assertEquals(2, node.getNodeNumber());
		Binding b = TransformUtils.nodeToBinding(plan, node, p);
		assertEquals("LoggerService_02", b.getName());
		assertEquals("b.machinename", "drh00282.ground.fedex.com",
				b.getMachine());

		// second node
		node = plan.getNodes().getNode().get(1);
		TransformUtils.applyGlobalDefinitions(plan.getNodes(), node, p);

		assertEquals("${componentName}_${nodeNumber}", node.getEngineBaseName());
		assertEquals("drh0028%s.ground.fedex.com", node.getMachineBaseName());
		assertEquals(8, node.getNodeNumber());
		b = TransformUtils.nodeToBinding(plan, node, p);
		assertEquals("LoggerService_08", b.getName());
		assertEquals("b.machinename", "drh00288.ground.fedex.com",
				b.getMachine());

		TransformUtils.interpolateProjectWithApplication(plan, application, p);
		List<Binding> bi = application.getServices().getBaseService().get(0)
				.getValue().getBindings().getBinding();
		assertEquals(2, bi.size());
		Binding bx = bi.get(0);
		assertEquals("drh00282.ground.fedex.com", bx.getMachine());
		bx = bi.get(1);
		assertEquals("drh00288.ground.fedex.com", bx.getMachine());

		// test marshalling... not sure what we are testing here :-)
		mojo.marshaller.marshal(application, new StreamResult(
				"/tmp/streamresult.xml"));
	}

	// @Test
	public void testNodeToBindingAndInterpolatingTwoNodesBasedOnGlobal()
			throws IntrospectionException {
		project = (Project) mojo.unmarshaller.unmarshal(new StreamSource(
				"src/test/resources/resource-filtering-two-nodes.xml"));
		DeploymentPlan plan = project.getPlans().getPlan().get(0);
		assertEquals(2, plan.getNumberOfNodes());

		Properties p = TransformUtils.extractGlobals(plan);

		// Expand
		plan.setNumberOfNodes(2);
		plan = TransformUtils.resizeNodes(plan);
		assertEquals(2, plan.getNodes().getNode().size());

		Node node = plan.getNodes().getNode().get(0);
		p.setProperty("nodeNumber", "01");
		// applyGlobalDefinitions.
		TransformUtils.applyGlobalDefinitions(plan.getNodes(), node, p);

		Binding b = TransformUtils.nodeToBinding(plan, node, p);

		assertEquals("LoggerService_01", b.getName());

		assertEquals("b.machinename", "drh00280.ground.fedex.com",
				b.getMachine());

		TransformUtils.interpolateProjectWithApplication(plan, application, p);
		List<Binding> bi = application.getServices().getBaseService().get(0)
				.getValue().getBindings().getBinding();
		assertEquals(2, bi.size());
		Binding bx1 = bi.get(0);
		assertEquals("drh00280.ground.fedex.com", bx1.getMachine());
		Binding bx2 = bi.get(1);
		assertEquals("ppbeggl02.ground.fede.com", bx2.getMachine());

		mojo.marshaller.marshal(application, new StreamResult(
				"/tmp/streamresult.xml"));
	}

	@Test
	public void testResizeNodesToPlan() throws IntrospectionException {
		DeploymentPlan plan = project.getPlans().getPlan().get(0);
		assertEquals(2, plan.getNumberOfNodes());

		assertEquals(2, plan.getNodes().getNode().size());
		plan = TransformUtils.resizeNodes(plan);
		assertEquals(2, plan.getNodes().getNode().size());

		// Expand
		plan.setNumberOfNodes(3);
		plan = TransformUtils.resizeNodes(plan);
		assertEquals(3, plan.getNodes().getNode().size());

		// Slim-Down
		plan.setNumberOfNodes(1);
		plan = TransformUtils.resizeNodes(plan);
		assertEquals(1, plan.getNodes().getNode().size());

		// Expand
		plan.setNumberOfNodes(3);
		plan = TransformUtils.resizeNodes(plan);
		assertEquals(3, plan.getNodes().getNode().size());
		Node nx = plan.getNodes().getNode().get(0);
		assertEquals(1, nx.getNodeNumber());
		nx = plan.getNodes().getNode().get(1);
		assertEquals(2, nx.getNodeNumber());
		nx = plan.getNodes().getNode().get(2);
		assertEquals(3, nx.getNodeNumber());
	}
}
