package scm.tibco.plugins;

import static org.junit.Assert.assertEquals;

import java.io.File;

import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.artifact.DefaultArtifact;
import org.junit.Before;
import org.junit.Test;

public class ArtifactItemTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		Artifact a = new DefaultArtifact("com.fedex.sefs.common:sefs_suBWLogging:projlib:1.0.0-SNAPSHOT");
		ArtifactItem item =new ArtifactItem(a);
		
		assertEquals("com.fedex.sefs.common:sefs_suBWLogging:1.0.0-SNAPSHOT:projlib",item.toString());
		assertEquals("./sefs_suBWLogging-1.0.0-SNAPSHOT.projlib", item.toFile("."));
		
		item.setOutputDirectory(new File("target/classes") );
		assertEquals("target/classes/sefs_suBWLogging-1.0.0-SNAPSHOT.projlib", item.toFile("target/classes"));
		item.setDestFileName("bw-logging.jar");
		assertEquals("target/classes/bw-logging.jar", item.toFile("target/classes"));
		item.setDestFileName("bw-logging.jar");
		assertEquals("/var/tmp/target/classes/bw-logging.jar", item.toFile("/var/tmp", "target/classes"));
		
		

	}
	
	@Test
	public void test2()
	{
		Artifact a = new DefaultArtifact("com.oracle:ojdbc6:jar:11.2.0.3");
		ArtifactItem item =new ArtifactItem(a);
		
		assertEquals("com.oracle:ojdbc6:11.2.0.3:jar",item.toString());
		assertEquals("./ojdbc6-11.2.0.3.jar", item.toFile("."));
		
	}

}
