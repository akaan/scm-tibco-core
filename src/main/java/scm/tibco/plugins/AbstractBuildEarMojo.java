/**
 * 
 */
package scm.tibco.plugins;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Parameter;
import org.eclipse.aether.repository.RemoteRepository;

import scm.core.ScmWorkingRepository;
import scm.tibco.plugins.events.UploadProjectEventImpl;

/**
 * @author akaan
 *
 */
public abstract class AbstractBuildEarMojo extends AbstractTibcoGlobalMojo {

	/**
	 * Sets the BuildEar operation:
	 * 
	 * <pre>
	 *               &lt;buildEar&gt;
	 *                 &lt;!-- Final name of the ear --&gt;
	 *                 &lt;finalName&gt;SURoadsFeed&lt;/finalName&gt;
	 *                 &lt;!-- Remote host with tibco tools and staging area. --&gt;
	 *                 &lt;adminRepository&gt;scpexe://irh00394.ute.fedex.com/BUILD/SUROADSFeed&lt;/adminRepository&gt;
	 *                 &lt;!-- Business Works archive. --&gt;
	 *                 &lt;archiveUri&gt;/ROADS_PackAssign.archive&lt;/archiveUri&gt;
	 *                 &lt;!-- Specifies where to place the generated alias properties. --&gt;
	 *                 &lt;aliasDirectory&gt;${build.dir}/SUROADSFeed/target/classes/&lt;/aliasDirectory&gt;
	 *                 &lt;!-- Specifies the root of the Business Works project directory. --&gt;
	 *                 &lt;projectDirectory&gt;${build.dir}/SUROADSFeed/SUROADSFeedSrc&lt;/projectDirectory&gt;
	 *                 &lt;!-- Indicates whether the remote staging area gets cleaned. --&gt;
	 *                 &lt;clean&gt;true&lt;/clean&gt;
	 *                 &lt;!-- Indicates overwriting previous resulsts --&gt;
	 *                 &lt;overwrite&gt;true&lt;/overwrite&gt;
	 *                 &lt;!-- Indicates processing aliases as maven dependencies --&gt;
	 *                 &lt;includeDependenciesAsAlias&gt;true&lt;/includeDependenciesAsAlias&gt;
	 *                 &lt;!-- Strip version of dependencies when generating aliases. 
	 *                 		For example: commons-io-1.2.jar =&gt; commons-io.jar 
	 *                  --&gt;
	 *                 &lt;stripVersionDependency&gt;true&lt;/stripVersionDependency&gt;
	 *                 &lt;!-- List of dependencies for the alias properties and inclusion with the project ear --&gt;
	 *                 &lt;dependencies&gt;
	 *                   &lt;dependency&gt;
	 *                     &lt;groupId&gt;com.fedex.eit&lt;/groupId&gt;
	 *                     &lt;artifactId&gt;fedexjms&lt;/artifactId&gt;
	 *                     &lt;version&gt;6.0.0&lt;/version&gt;
	 *                   &lt;/dependency&gt;
	 *                   &lt;dependency&gt;
	 *                     &lt;groupId&gt;com.fedex.eit&lt;/groupId&gt;
	 *                     &lt;artifactId&gt;jms&lt;/artifactId&gt;
	 *                     &lt;version&gt;6.0.0&lt;/version&gt;
	 *                   &lt;/dependency&gt;
	 *                 &lt;/dependencies&gt;
	 *               &lt;/buildEar&gt;
	 * </pre>
	 */
	@Parameter
	public AbstractBuildEar buildEar;
	/**
	 * Collection of buildear archives (see buildEar for more details).
	 */
	@Parameter
	public List<AbstractBuildEar> buildEars = new ArrayList<AbstractBuildEar>();
	/**
	 * The flag for uploading the project to remote location.
	 */
	@Parameter(defaultValue = "true")
	protected boolean uploadProject = true;
	/**
	 * The source directory for the Business Works project.
	 */
	@Parameter()
	protected List<String> sourceDirectories;
	@Parameter(defaultValue = "${basedir}")
	protected File baseDir;
	/**
	 * Cleaning the remote project directory per execution.
	 */
	@Parameter(defaultValue = "true")
	public boolean clean;
	protected long startTime = 0L;

	/**
	 * 
	 */
	public AbstractBuildEarMojo() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.maven.plugin.Mojo#execute()
	 */
	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		this.startTime = System.currentTimeMillis();

		if (buildEar == null && buildEars.size() == 0) {
			throw new MojoFailureException(
					"Must specify <buildEar></buildEar> in configuration section.");
		} else {
			buildEars = new ArrayList<AbstractBuildEar>();
			buildEars.add(buildEar);
		}

		if (buildEar != null && buildEars.size() != 0) {
			getLog().warn(
					"Please move your <buildEar></buildEar> in configuration section under <buildEars>.");
		}

		// initialize repository system.
		initializeRepositorySystem();

		this.background = false;

		//
		int count = 1;
		getLog().info(">>>>>>found " + buildEars.size() + " ear build(s).");
		for (Iterator<AbstractBuildEar> iterator = buildEars.iterator(); iterator
				.hasNext();) {
			AbstractBuildEar build = iterator.next();
			buildEar(build, count++);
		}

		long time = System.currentTimeMillis();
		message("operation took " + (time - startTime) / 1000 + "sec.");

	}

	/**
	 * 
	 * @param abstractBuildEar
	 * @param count
	 * @throws MojoExecutionException
	 * @throws MojoFailureException
	 */
	public File buildEar(AbstractBuildEar abstractBuildEar, int count)
			throws MojoExecutionException, MojoFailureException {

		// setting a new repository system.
		try {
			if (abstractBuildEar.adminRepository != null) {
				this.adminRepository = abstractBuildEar.adminRepository;
			}
			if (this.adminRepository == null) {
				throw new MojoExecutionException(
						"Must specify 'adminRepository' as a configuration.");
			}
			message("Switching to repository #" + count + " "
					+ this.adminRepository);
			this.setAdminRepository(this.adminRepository);
			initializeWagon(this.wagonManager);

			List<ArtifactItem> list = resolveArtifactItems(abstractBuildEar);
			afterResolveDependencies(list);

			// upload the project to the repository system
			if (uploadProject) {

				for (Iterator<String> iterator = sourceDirectories.iterator(); iterator
						.hasNext();) {
					String file = (String) iterator.next();
					UploadProjectEventImpl uploadEvent = new UploadProjectEventImpl(
							this, getAdminRepositorySystem(), baseDir, file,
							clean);
					try {
						uploadEvent.call();
					} catch (Exception e) {
						throw new MojoFailureException(e.getMessage(), e);
					}
				}
			} else {
				getLog().warn("skipping uploading the project!!");
			}

			// // building the final ear
			return buildEarEvent(this.getAdminRepositorySystem(),
					abstractBuildEar.finalName == null ? this.finalName
							: abstractBuildEar.finalName, abstractBuildEar);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			getLog().error(e1);
			throw new MojoExecutionException(e1.getMessage(), e1);
		}
	}

	public void initializeWagon() throws MojoExecutionException {
		initializeWagon(this.wagonManager);
	}

	public abstract List<ArtifactItem> resolveArtifactItems(
			AbstractBuildEar buildEar) throws MojoFailureException;

	public abstract void afterResolveDependencies(
			List<ArtifactItem> artifactItems) throws Exception;

	/**
	 * 
	 * @param scmWorkingRepository
	 * @param string
	 * @param abstractBuildEar2
	 * @return
	 */
	public abstract File buildEarEvent(
			ScmWorkingRepository scmWorkingRepository, String finalName,
			AbstractBuildEar abstractBuildEar) throws MojoExecutionException;

	/**
	 * 
	 * @param list
	 * @return
	 */
	public List<RemoteRepository> toRemoteRepositories(
			List<ArtifactRepository> list) {
		List<RemoteRepository> rr = new ArrayList<RemoteRepository>();
		for (Iterator iterator = list.iterator(); iterator.hasNext();) {
			ArtifactRepository artifactRepository = (ArtifactRepository) iterator
					.next();
			RemoteRepository newRepo = new RemoteRepository.Builder(
					artifactRepository.getId(), artifactRepository.getKey(),
					artifactRepository.getUrl()).build();
			rr.add(newRepo);
			getLog().info("add resolving repository " + newRepo);
		}
		return rr;
	}

}
