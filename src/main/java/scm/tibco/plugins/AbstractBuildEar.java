/**
 * 
 */
package scm.tibco.plugins;

import java.io.File;
import java.util.List;

import org.apache.maven.model.Dependency;
import org.apache.maven.plugins.annotations.Parameter;

/**
 * @author akaan
 * 
 */
public class AbstractBuildEar {

	/*
	 * /opt/tibco/tra/5.8/bin/buildear -x -a
	 * /opt/fedex/FSD/BUILD/grd_fsd_bpms_CPC
	 * /grd_fsd_bpms_CPC/GRD_FSD_BPMS/CPC/Pickup/target/alias.properties -ear
	 * /PickupCommon/Deployments/PickupOfferAssignmentProject.archive -p
	 * /opt/fedex
	 * /FSD/BUILD/grd_fsd_bpms_CPC/grd_fsd_bpms_CPC/GRD_FSD_BPMS/CPC/Pickup
	 * /src/main/PickupOfferAssignmentProject -o
	 * /opt/fedex/FSD/BUILD/grd_fsd_bpms_CPC
	 * /grd_fsd_bpms_CPC/GRD_FSD_BPMS/CPC/Pickup
	 * /target/PickupOfferAssignmentProject-2.ear
	 */

	@Parameter
	public String id;

	/**
	 * /** The administration host and the directory where we transfer the
	 * archive (ear) and use as a working directory. The format is
	 * [hostname]:[directory] example: scpexe://home.prod.ux.com:/tmp/myapp
	 */
	@Parameter
	public String adminRepository;

	@Parameter(defaultValue = "${project.build.finalName}")
	public String finalName;


	@Parameter
	public List<ArtifactItem> artifactItems;

	/**
	 * The buildEar working directory for uploading and downloading files for
	 * the plugin.
	 */
	@Parameter(defaultValue = "${project.build.sourceDirectory}/MyApp")
	public File projectDirectory;

	@Parameter
	public String outputDirectory = "target/classes";
	
	/**
	 * Cleaning the remote project directory.
	 */
	@Parameter(defaultValue = "true")
	public boolean clean;

	@Parameter(defaultValue = "true")
	public boolean includeDependenciesAsAlias;

	/**
	 * The buildEar working directory for uploading and downloading files for
	 * the plugin.
	 */
	@Parameter(defaultValue = "/Project/Deployment/MyApp.archive")
	public String archiveUri;



	/**
	 * BE parameter:
	 */
	@Parameter(defaultValue = "default.cdd")
	public String cddFile;
	
	/**
	 * -a (optional) specify a properties file containing aliases for DTL
	 * library locations. Example:
	 * ${project.build.outputDirectory}/alias.properties
	 */
	@Parameter(defaultValue = "${project.build.directory}/classes")
	public File aliasDirectory;

	@Parameter(defaultValue = "true")
	public boolean stripVersionDependency;
	/**
	 * -s (optional) saves the project after the archive is created (project
	 * must be writable).
	 */
	@Parameter
	public boolean save = true;
	/**
	 * -v (optional) validate project before creating archive. Aborts on errors.
	 * Warnings are ignored.
	 */
	@Parameter
	public boolean validate = false;
	/**
	 * -x (optional) overwrites the specified output file if it exists.
	 */
	@Parameter
	public boolean overwrite;

	/**
	 * 
	 */
	public AbstractBuildEar() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "BuildEar [id=" + id + ", adminRepository=" + adminRepository
				+ ", finalName=" + finalName + ", projectDirectory="
				+ projectDirectory
				+ ", clean=" + clean + ", includeDependenciesAsAlias="
				+ includeDependenciesAsAlias + ", archiveUri=" + archiveUri
				+ ", aliasDirectory=" + aliasDirectory
				+ ", stripVersionDependency=" + stripVersionDependency
				+ ", save=" + save + ", validate=" + validate + ", overwrite="
				+ overwrite + "]";
	}


}
