/**
 * 
 */
package scm.tibco.plugins.events;

import java.io.File;
import java.io.FileReader;
import java.util.Iterator;
import java.util.Properties;

import scm.core.event.ActionPropertyReadEvent;
import scm.tibco.plugins.AbstractTibcoMojo;
import scm.tibco.plugins.Target;
import scm.tibco.plugins.Target.GlobalVariableSet;
import scm.tibco.plugins.TibcoRemoteExecutor;

/**
 * @author akaan
 *
 */
public class LoadGlobalVariablesEvent extends TibcoRemoteExecutor implements
		ActionPropertyReadEvent {

	private final Target target;
	/**
	 * @param aPlugin
	 */
	public LoadGlobalVariablesEvent(AbstractTibcoMojo aPlugin, Target target) {
		super(aPlugin);
		this.target = target;
	}

	@Override
	public Properties call() throws Exception {
		message( this.getClass().getSimpleName() + ": Load Global Variables");
		Properties properties = new Properties();
		if (target.globalVariableSet != null) {
			GlobalVariableSet set = target.globalVariableSet;
			if (set.globalVariableRefs != null) {
				for (Iterator<File> iterator = set.globalVariableRefs.iterator(); iterator
						.hasNext();) {
					File type = (File) iterator.next();
					properties.load(new FileReader(type));
				}
			}
			if (set.globalVariables != null ) {
				properties.putAll(set.globalVariables);
			}
		}
		return properties;
	}

}
