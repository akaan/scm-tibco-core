/**
 * 
 */
package scm.tibco.plugins.events;

import java.io.File;

import scm.core.event.ActionVoidEvent;
import scm.tibco.plugins.AbstractTibcoMojo;
import scm.tibco.plugins.TibcoRemoteExecutor;

/**
 * @author akaan
 * 
 */
public class DownloadEarEventImpl extends TibcoRemoteExecutor implements
		ActionVoidEvent {

	private final File remoteOutputDirectory;
	private final String finalName;

	/**
	 * 
	 * @param plugin
	 * @param remoteOutputDirectory
	 * @param finalName
	 */
	public DownloadEarEventImpl(AbstractTibcoMojo plugin,
			File remoteOutputDirectory, String finalName) {
		super(plugin);
		assert remoteOutputDirectory != null;
		assert finalName != null;
		this.remoteOutputDirectory = remoteOutputDirectory;
		this.finalName = finalName + ".ear";
	}

	/**
	 * 
	 */
	@Override
	public Void call() throws Exception {
		// by default the mojo hostAndDirectory location is used.
		getWagon().put(
				new File(this.remoteOutputDirectory, this.finalName), "");
		return null;
	}
	
}
