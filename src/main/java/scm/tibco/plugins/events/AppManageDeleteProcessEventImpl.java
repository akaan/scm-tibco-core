/**
 * 
 */
package scm.tibco.plugins.events;

import scm.core.event.ActionVoidEvent;
import scm.tibco.plugins.AbstractTibcoMojo;
import scm.tibco.plugins.CommandCreator;
import scm.tibco.plugins.TibcoRemoteExecutor;

import com.fedex.scm.tibco.project.DeploymentPlan;
import com.tibco.xmlns.applicationmanagement.ApplicationType;

/**
 * @author akaan
 * 
 */
public class AppManageDeleteProcessEventImpl extends TibcoRemoteExecutor
		implements ActionVoidEvent {

	private final DeploymentPlan plan;

	/**
	 * @param mojo
	 */
	public AppManageDeleteProcessEventImpl(AbstractTibcoMojo plugin,
			ApplicationType application, DeploymentPlan plan) {
		super(plugin);
		this.plan = plan;
	}

	public Void call() throws Exception {
		message(this.getClass().getSimpleName() + " : Deleting Process "
				+ plan.getProjectName());
		String command = CommandCreator.createAppManageDeleteProcess(
				getPlugin().getTibcoHome(), getPlugin().getTibcoVersion(),
				plan.getProjectName(), plan.getDomainName(),
				plan.getAdminUsername(), plan.getAdminPassword());
		getLog().debug(command);
		getPlugin().executeCommandExec(getWagon(), command);
		return null;
	}
}
