/**
 * 
 */
package scm.tibco.plugins.events;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.plexus.util.FileUtils;
import org.eclipse.aether.DefaultRepositorySystemSession;
import org.eclipse.aether.RepositorySystem;
import org.eclipse.aether.RepositorySystemSession;
import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.artifact.DefaultArtifact;
import org.eclipse.aether.repository.LocalRepository;
import org.eclipse.aether.repository.RemoteRepository;
import org.eclipse.aether.resolution.ArtifactRequest;
import org.eclipse.aether.resolution.ArtifactResult;

import scm.tibco.plugins.AbstractTibcoGlobalMojo;
import scm.tibco.plugins.ArtifactItem;
import scm.tibco.plugins.TibcoRemoteExecutor;

/**
 * @author akaan
 *
 */
public class ResolveArtifactItemsEventImpl extends TibcoRemoteExecutor
		implements ActionArtifactItemListEvent {

	private final List<ArtifactItem> artifactItems;
	private final RepositorySystem repoSystem;
	private final List<RemoteRepository> repositories;
	private final String outputDirectory;
	private final File baseDir;

	/**
	 * 
	 * @param baseDir
	 * @param buildEarPlugin
	 * @param project
	 * @param artifactItems
	 */
	public ResolveArtifactItemsEventImpl(AbstractTibcoGlobalMojo plugin,
			List<RemoteRepository> repositories, File baseDir,
			List<ArtifactItem> artifactItems, RepositorySystem repoSystem,
			String outputDirectory, boolean debug) {
		super(plugin);
		assert artifactItems != null;
		assert repoSystem != null;
		assert repositories != null;
		this.artifactItems = artifactItems;
		this.repoSystem = repoSystem;
		this.repositories = repositories;
		this.outputDirectory = outputDirectory;
		this.baseDir = baseDir;
	}

	@Override
	public List<ArtifactItem> call() throws Exception {
		RepositorySystemSession session = newSession();
		List<ArtifactItem> resolved = new ArrayList<ArtifactItem>();
		for (ArtifactItem artifactItem : artifactItems) {
			getLog().info("including dependency: " + artifactItem.toString());

			// if the output directory is explicitly set
			// in the artifact item use it.
			if (artifactItem.getOutputDirectory() == null) {
				artifactItem.setOutputDirectory(new File(this.baseDir,
						this.outputDirectory));
			}
			// get maven artifact representing this mojo artifact
			ArtifactResult artifactResult = null;
			try {
				String mgtKey = String.format("%s:%s:%s:%s",
						artifactItem.getGroupId(),
						artifactItem.getArtifactId(), artifactItem.getType(),
						artifactItem.getVersion());
				Artifact artifact = new DefaultArtifact(mgtKey);
				getLog().info("finding artifact " + mgtKey);

				ArtifactRequest ar = new ArtifactRequest(artifact,
						this.repositories, "local-repo");
				artifactResult = repoSystem.resolveArtifact(session, ar);

				//
				// if (artifactResult.isResolved()) {
				artifactItem.setArtifact(artifactResult.getArtifact());
				resolved.add(artifactItem);
				// }
			} catch (Exception e) {
				getLog().error("failed finding artifact: " + e.getMessage(), e);
			}
			if (artifactResult != null) {
				getLog().info(
						"collected: "
								+ artifactResult.getArtifact().getFile()
								+ " to "
								+ artifactItem.toFile(
										this.baseDir.getAbsolutePath(),
										this.outputDirectory));
				FileUtils.copyFile(artifactResult.getArtifact().getFile(),
						new File(artifactItem.toFile(this.baseDir.getAbsolutePath(),
								this.outputDirectory)));
			}
		}
		getLog().debug(
				"------------------- end of resolve dependencies ----------------->\n");
		getLog().debug("resolved [" + resolved + "]\n");

		return resolved;
	}

	/**
	 * 
	 * @return
	 */
	private RepositorySystemSession newSession() {
		DefaultRepositorySystemSession session = new DefaultRepositorySystemSession();
		LocalRepository localRepo = new LocalRepository("target/local-repo");
		session.setLocalRepositoryManager(this.repoSystem
				.newLocalRepositoryManager(session, localRepo));
		return session;
	}
}
