/**
 * 
 */
package scm.tibco.plugins.events;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.maven.model.Dependency;
import org.eclipse.aether.DefaultRepositorySystemSession;
import org.eclipse.aether.RepositorySystem;
import org.eclipse.aether.RepositorySystemSession;
import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.artifact.DefaultArtifact;
import org.eclipse.aether.repository.LocalRepository;
import org.eclipse.aether.repository.RemoteRepository;
import org.eclipse.aether.resolution.ArtifactRequest;
import org.eclipse.aether.resolution.ArtifactResult;

import scm.core.event.ActionFileListEvent;
import scm.tibco.plugins.AbstractTibcoGlobalMojo;
import scm.tibco.plugins.TibcoRemoteExecutor;

/**
 * @author akaan
 *
 */
public class ResolveDependenciesEventImpl extends TibcoRemoteExecutor implements
		ActionFileListEvent {

	private final List<Dependency> dependencies;
	private final RepositorySystem repoSystem;
	private final List<RemoteRepository> repositories;

	/**
	 * 
	 * @param buildEarPlugin
	 * @param project
	 * @param dependencies
	 */
	public ResolveDependenciesEventImpl(AbstractTibcoGlobalMojo plugin,
			List<RemoteRepository> repositories, List<Dependency> dependencies,
			RepositorySystem repoSystem, boolean debug) {
		super(plugin);
		assert dependencies != null;
		assert repoSystem != null;
		assert repositories != null;
		this.dependencies = dependencies;
		this.repoSystem = repoSystem;
		this.repositories = repositories;
	}

	@Override
	public List<File> call() throws Exception {
		RepositorySystemSession session = newSession();
		List<Artifact> resolved = new ArrayList<Artifact>();
		for (Dependency dependency : dependencies) {
			getLog().info("Including dependency: " + dependency.toString());
			// get maven artifact representing this mojo artifact
			ArtifactResult artifactResult = null;
			try {
				String mgtKey = String.format("%s:%s:%s:%s",
						dependency.getGroupId(), dependency.getArtifactId(),
						dependency.getType(), dependency.getVersion());
				Artifact artifact = new DefaultArtifact(mgtKey);
				getLog().info("Finding artifact " + mgtKey);

				ArtifactRequest ar = new ArtifactRequest(artifact, this.repositories, "local-repo");
				artifactResult = repoSystem.resolveArtifact(session, ar);
				//
				// if (artifactResult.isResolved()) {
				resolved.add(artifactResult.getArtifact());
				// }
			} catch (Exception e) {
				getLog().error("failed finding artifact: " + e.getMessage());
				continue;
			}
			if (artifactResult != null) {
				getLog().info(
						">>>>Collected: "
								+ artifactResult.getArtifact().getFile()
								+ " to "
								+ artifactResult.getArtifact().getFile());
			}
		}
		getLog().debug(
				"------------------- end of resolve dependencies ----------------->\n");
		getLog().debug("resolved [" + resolved + "]\n");

		return toFileList(resolved);
	}

	/**
	 * 
	 * @param artifacts
	 * @return
	 */
	private static List<File> toFileList(List<Artifact> artifacts) {
		List<File> files = new ArrayList<File>();
		for (Iterator<Artifact> iterator = artifacts.iterator(); iterator
				.hasNext();) {
			Artifact file = (Artifact) iterator.next();
			files.add(file.getFile());
		}
		return files;
	}

	/**
	 * 
	 * @return
	 */
	private RepositorySystemSession newSession() {
		DefaultRepositorySystemSession session = new DefaultRepositorySystemSession();
		LocalRepository localRepo = new LocalRepository("target/local-repo");
		session.setLocalRepositoryManager(this.repoSystem
				.newLocalRepositoryManager(session, localRepo));
		return session;
	}
}
