/**
 * 
 */
package scm.tibco.plugins.events;

import java.io.File;

import org.codehaus.plexus.util.FileUtils;

import scm.core.ScmWorkingRepository;
import scm.core.event.ActionTextEvent;
import scm.tibco.plugins.AbstractTibcoMojo;
import scm.tibco.plugins.CommandCreator;
import scm.tibco.plugins.TibcoRemoteExecutor;
import scm.tibco.plugins.TransformUtils;

/**
 * @author akaan
 * 
 */
public class TailLogImpl extends TibcoRemoteExecutor implements ActionTextEvent {

	private final String logFileName;
	final ScmWorkingRepository adminRepository;
	final File stagingDirectory;

	/**
	 * 
	 * @param logFileName
	 * @param aPlugin
	 * @param adminRepository
	 * @param stagingDirectory
	 */
	public TailLogImpl(String logFileName, AbstractTibcoMojo aPlugin,
			ScmWorkingRepository adminRepository, File stagingDirectory) {
		super(aPlugin);
		assert logFileName != null;
		assert adminRepository != null;
		assert stagingDirectory != null;
		this.logFileName = logFileName;
		this.adminRepository = adminRepository;
		this.stagingDirectory = stagingDirectory;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.concurrent.Callable#call()
	 */
	@Override
	public String call() throws Exception {
		File tempFileLocal = File.createTempFile(logFileName + "-", ".log",
				this.stagingDirectory);

		File remoteWorkingFile = TransformUtils
				.createRemoteWorkDirectoryAndTempTextFile(this.adminRepository,
						false);
		message("Tail log of " + this.logFileName + "\n       |    dest => "
				+ tempFileLocal.getAbsolutePath() + "\n       |    trgt => "
				+ remoteWorkingFile.getAbsolutePath(), false);
		String command = CommandCreator.createAppManageTailLog(getPlugin()
				.getTibcoHome(), getPlugin().getTibcoVersion(),
				this.logFileName, remoteWorkingFile);
		// Wagon wagon = mojo.getWagon(this.mojo.getWagonManager(),
		// this.mojo.getUser(), this.mojo.getHostAndDirectory());
		getPlugin().executeCommandExec(getWagon(), command);
		getWagon().get(remoteWorkingFile.getName(), tempFileLocal);
		String message = FileUtils.fileRead(tempFileLocal);
		getLog().info(
				"======================================REMOTE==LOG=======================================");
		getLog().info(message);
		getLog().info(
				"=====================================END=REMOTE=LOG=====================================");
		return message;
	}

}
