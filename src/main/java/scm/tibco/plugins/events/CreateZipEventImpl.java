/**
 * 
 */
package scm.tibco.plugins.events;

import java.io.File;
import java.io.FileNotFoundException;

import org.codehaus.plexus.util.FileUtils;

import scm.core.event.ActionFileEvent;
import scm.tibco.plugins.AbstractTibcoGlobalMojo;
import scm.tibco.plugins.AbstractTibcoMojo;
import scm.tibco.plugins.ArchiveUtilities;
import scm.tibco.plugins.TibcoRemoteExecutor;
import edu.emory.mathcs.backport.java.util.Arrays;

/**
 * @author akaan
 *
 */
public class CreateZipEventImpl extends TibcoRemoteExecutor implements
		ActionFileEvent {

	private final File stagingDirectory;
	private final File earFile;
	private final File configurationXML;
	private final String packageNameForSilverFabric;

	/**
	 * 
	 */
	public CreateZipEventImpl(
			AbstractTibcoGlobalMojo appManageDeploymentPlugin,
			File stagingDirectory, File earFile, File configurationXML,
			String packageNameForSilverFabric) {
		super(appManageDeploymentPlugin);
		this.stagingDirectory = stagingDirectory;
		this.earFile = earFile;
		this.configurationXML = configurationXML;
		this.packageNameForSilverFabric = packageNameForSilverFabric;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.concurrent.Callable#call()
	 */
	@Override
	public File call() throws Exception {
		message("Create Zip Archive BW for " + this.earFile.getName());

		if (!this.earFile.exists()) {
			throw new FileNotFoundException("The EAR File " + this.earFile
					+ " does not exist!");
		}
		if (!this.configurationXML.exists()) {
			throw new FileNotFoundException(
					"The Deployment Configuration File "
							+ this.configurationXML + " does not exist!");
		}
		
		String earZipName = this.earFile.getName();
		earZipName = earZipName.replace(".ear", "");

		File sfArchive = new File(this.stagingDirectory, earZipName
				+ ".zip");

		File sfConfig = new File(
				this.configurationXML.getParent(),
				(this.packageNameForSilverFabric != null
						&& this.packageNameForSilverFabric != "" ? this.packageNameForSilverFabric
						: this.earFile.getName() + ".xml"));
		FileUtils.copyFile(configurationXML, sfConfig);
		return ArchiveUtilities.createJarFile(sfArchive,
				Arrays.asList(new File[] { earFile, sfConfig }));
	}

//	/**
//	 * @param srcJarFile
//	 * @param targetPackage
//	 * @param filesToAdd
//	 * @throws IOException
//	 */
//	public static File createJarFile(File srcJarFile, List<File> filesToAdd)
//			throws IOException {
//		boolean jarUpdated = false;
//
//		try {
//			JarOutputStream tempJarOutputStream = new JarOutputStream(
//					new FileOutputStream(srcJarFile));
//
//			try {
//				// Added the new files to the jar.
//				for (int i = 0; i < filesToAdd.size(); i++) {
//					File file = (File) filesToAdd.get(i);
//					FileInputStream fis = new FileInputStream(file);
//					try {
//						byte[] buffer = new byte[1024];
//						int bytesRead = 0;
//						JarEntry entry = new JarEntry(file.getName());
//						tempJarOutputStream.putNextEntry(entry);
//						while ((bytesRead = fis.read(buffer)) != -1) {
//							tempJarOutputStream.write(buffer, 0, bytesRead);
//						}
//
//						System.out.println(entry.getName() + " added to "
//								+ srcJarFile + ".");
//					} finally {
//						fis.close();
//					}
//				}
//			} catch (Exception ex) {
//				ex.printStackTrace();
//				tempJarOutputStream.putNextEntry(new JarEntry("stub"));
//			} finally {
//				tempJarOutputStream.close();
//			}
//
//		} finally {
//			System.out.println(srcJarFile.getAbsolutePath() + " closed.");
//
//		}
//		System.out.println(srcJarFile.getAbsolutePath() + " created.");
//		return srcJarFile;
//	}

}
