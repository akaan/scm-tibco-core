/**
 * 
 */
package scm.tibco.plugins.events;

import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.JAXBElement;

import scm.core.event.ActionVoidEvent;
import scm.tibco.plugins.AbstractTibcoMojo;
import scm.tibco.plugins.TibcoRemoteExecutor;

import com.tibco.xmlns.applicationmanagement.ApplicationType;
import com.tibco.xmlns.applicationmanagement.NVPairType;

/**
 * @author akaan
 *
 */
public class GlobalVariableMergeEvent extends TibcoRemoteExecutor implements
		ActionVoidEvent {

	/**
	 * 
	 */
	private final Properties nameValuePairs;
	private final ApplicationType application;

	/**
	 * @param aPlugin
	 */
	public GlobalVariableMergeEvent(AbstractTibcoMojo aPlugin,
			Properties globalNameValuePairs, ApplicationType application) {
		super(aPlugin);
		assert globalNameValuePairs != null;
		assert application != null;
		this.nameValuePairs = globalNameValuePairs;
		this.application = application;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.concurrent.Callable#call()
	 */
	@Override
	public Void call() throws Exception {
		
		message(this.getClass().getSimpleName() + ": Merging Global Variables: " + application.getName(), true);
		List<JAXBElement<? extends NVPairType>> lnv = application.getNVPairs()
				.getNVPair();

		for (Iterator<JAXBElement<? extends NVPairType>> iterator = lnv
				.iterator(); iterator.hasNext();) {
			NVPairType nvPairsType = iterator.next().getValue();
			if (nameValuePairs.containsKey(nvPairsType.getName())) {
				nvPairsType.setValue(nameValuePairs.getProperty(nvPairsType
						.getName()));
			}
		}
		return null;
	}

}
