/**
 * 
 */
package scm.tibco.plugins.events;

import scm.core.event.ActionVoidEvent;
import scm.tibco.plugins.AbstractTibcoGlobalMojo;
import scm.tibco.plugins.AbstractTibcoMojo;
import scm.tibco.plugins.CommandCreator;
import scm.tibco.plugins.TibcoRemoteExecutor;

import com.fedex.scm.tibco.project.DeploymentPlan;

/**
 * @author akaan
 * 
 */
public class AppManageUndeployEventImpl extends TibcoRemoteExecutor implements
		ActionVoidEvent {

	final DeploymentPlan plan;

	/**
	 * @param aPlugin
	 */
	public AppManageUndeployEventImpl(AbstractTibcoMojo plugin,
			DeploymentPlan plan) {
		super(plugin);
		this.plan = plan;
	}

	@Override
	public Void call() throws Exception {
		message(this.getClass().getSimpleName() + " : Undeploying Application "
				+ plan.getProjectName());
		String command = AppManageUndeployEventImpl.createAppManageUndeploy(
				getPlugin(), plan.getProjectName(), plan.getDomainName(),
				plan.getAdminUsername(), plan.getAdminPassword());
		getLog().debug(command);
		getPlugin().executeCommandExec(getWagon(), command);
		return null;
	}

	/**
	 * <pre>
	 * No user name or password specified
	 * 
	 * Usage:  AppManage -undeploy -app <app> -domain <domain> -user <user> -pw <password> [-cred <cred>] [-timeout <timeout>]
	 *         (to undeploy an application)
	 * 
	 * where args include:
	 * <app>          name of an application (case sensitive)
	 *                May contain folder if the application is not top level
	 *                Folder name should be separated by slash "/"
	 * <domain>       name of a domain (case sensitive)
	 * <user>         name of an authorized user that has read/write permission of the application(s)
	 *                and read/write permission to access data of SYS_<domain>
	 * <password>     password of an authorized user (case sensitive)
	 *                Should not be encrypted
	 * <cred>         name of a property file containing user and encrypted password
	 *                Use obfuscate.exe to encrypt the property file
	 *                If specified, no -user and -pw should be specified
	 * <timeout>      If specified, kill the service within <timeout> seconds
	 * 
	 * Example:
	 * </pre>
	 * 
	 * @param mojo
	 * @param projectName
	 * @param domainName
	 * @param adminUsername
	 * @param adminPassword
	 * @return
	 */
	public static String createAppManageUndeploy(AbstractTibcoGlobalMojo plugin,
			String projectName, String domainName, String adminUsername,
			String adminPassword) {
		return CommandCreator.createAppManageUndeploy(plugin.getTibcoHome(),
				plugin.getTibcoVersion(), projectName, domainName,
				adminUsername, adminPassword);
	}

}
