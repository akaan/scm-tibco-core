/**
 * 
 */
package scm.tibco.plugins.events;

import java.io.File;
import java.io.FileNotFoundException;

import scm.core.event.ActionFileEvent;
import scm.tibco.plugins.AbstractTibcoMojo;
import scm.tibco.plugins.TibcoRemoteExecutor;

/**
 * @author akaan
 * 
 */
public class UploadEarEventImpl extends TibcoRemoteExecutor implements
		ActionFileEvent {

	private final File localStageDirectory;
	private final String finalName;

	/**
	 * @param earFile2
	 * @param mojo
	 */
	public UploadEarEventImpl(AbstractTibcoMojo plugin,
			File stageDirectory, String finalName) {
		super(plugin);
		assert finalName != null;
		assert stageDirectory != null;
		String path = stageDirectory.getAbsolutePath().replaceAll("[\\n\\r]", "");
		this.localStageDirectory = new File(path, finalName);
		this.finalName = finalName;
	}

	/**
	 * 
	 */
	@Override
	public File call() throws Exception {
		// by default the mojo hostAndDirectory location is used.
		message(this.getClass().getSimpleName() + ": Upload Ear File "
				+ this.localStageDirectory 
				+ "\n       |    to destination=> " + getWagon().getRepository());
		if (this.localStageDirectory.exists()) {
			getWagon().put(this.localStageDirectory, this.finalName);
		} else {
			throw new FileNotFoundException("Unable to find file "
					+ this.localStageDirectory);
		}
		return this.localStageDirectory;
	}

	protected String getStageDirectory() {
		return this.localStageDirectory.getAbsolutePath();
	}
}
