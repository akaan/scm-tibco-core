package scm.tibco.plugins.events;

import java.util.List;
import java.util.concurrent.Callable;

import scm.core.event.ActionEvent;
import scm.tibco.plugins.ArtifactItem;

public interface ActionArtifactItemListEvent extends ActionEvent, Callable<List<ArtifactItem>> {

}
