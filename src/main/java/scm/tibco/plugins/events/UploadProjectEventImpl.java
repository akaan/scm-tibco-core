/**
 * 
 */
package scm.tibco.plugins.events;

import java.io.File;

import scm.core.ScmWorkingRepository;
import scm.core.event.ActionVoidEvent;
import scm.tibco.plugins.AbstractTibcoGlobalMojo;
import scm.tibco.plugins.TibcoRemoteExecutor;

/**
 * @author akaan
 * 
 */
public class UploadProjectEventImpl extends TibcoRemoteExecutor implements
		ActionVoidEvent {

	final ScmWorkingRepository adminRepository;
	final String sourceDirectory;
	final File _sourceDirectoryRef;
	final boolean clean;

	/**
	 * @param adminRepository
	 * @param baseDir
	 * @param mojo
	 */
	public UploadProjectEventImpl(AbstractTibcoGlobalMojo plugin,
			ScmWorkingRepository adminRepository, File baseDir,
			String sourceDirectory, boolean clean) {
		super(plugin);
		assert adminRepository != null;
		assert sourceDirectory != null;
		File f = new File(sourceDirectory);
		if (!sourceDirectory.startsWith("/")) {
			f = new File(baseDir, sourceDirectory);
		}
		_sourceDirectoryRef = f;
		assert _sourceDirectoryRef.exists()
				&& _sourceDirectoryRef.isDirectory();
		this.adminRepository = adminRepository;
		this.clean = clean;
		this.sourceDirectory = sourceDirectory;
	}

	/**
	 * 
	 */
	@Override
	public Void call() throws Exception {
		// by default the mojo hostAndDirectory location is used.
		String sourceDir = this.adminRepository.getBasedir() + "/"
				+ sourceDirectory;

		if (clean) {
			message("Creating Remote Working Area " + sourceDir);
			getPlugin().executeCommandExec(
					getWagon(),
					"if [ -e " + sourceDir + " ]; then rm -fr " + sourceDir
							+ " ; fi  ");
		}
		getLog().info(
				"Uploading Project: " + this.sourceDirectory + "\n\ttarget = "
						+ this.adminRepository + "|" + sourceDirectory
						+ "\n\tref = " + this._sourceDirectoryRef);
		getWagon().putDirectory(this._sourceDirectoryRef, sourceDirectory);
		return null;
	}
}
