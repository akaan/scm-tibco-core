package scm.tibco.plugins;

import java.io.File;
import java.net.MalformedURLException;

import org.apache.maven.artifact.manager.WagonManager;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.apache.maven.repository.internal.MavenRepositorySystemUtils;
import org.apache.maven.wagon.Wagon;
import org.eclipse.aether.RepositorySystem;
import org.eclipse.aether.connector.basic.BasicRepositoryConnectorFactory;
import org.eclipse.aether.impl.DefaultServiceLocator;
import org.eclipse.aether.spi.connector.RepositoryConnectorFactory;
import org.eclipse.aether.spi.connector.transport.TransporterFactory;
import org.eclipse.aether.transport.file.FileTransporterFactory;
import org.eclipse.aether.transport.http.HttpTransporterFactory;

import scm.core.AbstractRemoteInvocationMojo;
import scm.core.ScmWorkingRepository;

public abstract class AbstractTibcoGlobalMojo extends
		AbstractRemoteInvocationMojo {

	/**
	 * /** The administration host and the directory where we transfer the
	 * archive (ear) and use as a working directory. The format is
	 * [hostname]:[directory] example: scpexe://home.prod.ux.com:/tmp/myapp
	 */
	@Parameter(defaultValue = "localhost:/tmp/", required = true)
	protected String adminRepository;

	// repository system of which we extract the repositories for remote
	// invokation.
	protected ScmWorkingRepository adminRepositorySystem;

	protected RepositorySystem system;
	/**
	 * The TIBCO_HOME variable
	 */
	@Parameter(defaultValue = "/opt/tibco", required = true)
	protected File tibcoHome;

	/**
	 * The version of the tra installation.
	 */
	@Parameter(defaultValue = "5.8", required = true)
	protected String tibcoVersion;

	@Component
	protected Wagon wagon;

	public AbstractTibcoGlobalMojo() {
		super();
	}

	/**
	 * @return the adminRepository
	 */
	public final ScmWorkingRepository getAdminRepositorySystem() {
		if (adminRepositorySystem == null) {
			try {
				setAdminRepositorySystem(this.adminRepository);
			} catch (MalformedURLException e) {
				getLog().error(e);
			}
		}
		return adminRepositorySystem;
	}

	/**
	 * @return the project
	 */
	public final MavenProject getProject() {
		return project;
	}

	/**
	 * @return the system
	 */
	public final RepositorySystem getSystem() {
		return system;
	}

	/**
	 * @return the tibcoHome
	 */
	public final File getTibcoHome() {
		return tibcoHome;
	}

	/**
	 * @return the tibcoVersion
	 */
	public final String getTibcoVersion() {
		return tibcoVersion;
	}

	/**
	 * @return the wagon
	 */
	public final Wagon getWagon() {
		return wagon;
	}

	/**
	 * 
	 */
	public void initializeRepositorySystem() {
		org.apache.maven.repository.internal.MavenAetherModule module = null;
		DefaultServiceLocator locator = MavenRepositorySystemUtils
				.newServiceLocator();
		locator.addService(RepositoryConnectorFactory.class,
				BasicRepositoryConnectorFactory.class);
		locator.addService(TransporterFactory.class,
				FileTransporterFactory.class);
		locator.addService(TransporterFactory.class,
				HttpTransporterFactory.class);
		system = locator.getService(RepositorySystem.class);
	}

	/**
	 * 
	 * @throws MojoExecutionException
	 */
	public void initializeWagon(WagonManager wagonManager)
			throws MojoExecutionException {
		this.wagon = getWagon(wagonManager, getUser(),
				this.getAdminRepositorySystem());
		getLog().info(
				"Calculated repository to : " + this.adminRepositorySystem);
	}

	/**
	 * @param adminRepository
	 *            the adminRepository to set
	 * @throws MalformedURLException
	 */
	public final void setAdminRepository(String adminRepository)
			throws MalformedURLException {
		this.adminRepository = adminRepository;
		setAdminRepositorySystem(adminRepository);
	}

	/**
	 * @param adminRepository
	 *            the adminRepository to set
	 * @throws MalformedURLException
	 */
	public final void setAdminRepositorySystem(String adminRepositoryUrl)
			throws MalformedURLException {
		this.adminRepositorySystem = new ScmWorkingRepository(this,
				adminRepositoryUrl);
	}

	/**
	 * @param tibcoHome
	 *            the tibcoHome to set
	 */
	public final void setTibcoHome(File tibcoHome) {
		this.tibcoHome = tibcoHome;
	}

	/**
	 * @param tibcoVersion
	 *            the tibcoVersion to set
	 */
	public final void setTibcoVersion(String tibcoVersion) {
		this.tibcoVersion = tibcoVersion;
	}
}
