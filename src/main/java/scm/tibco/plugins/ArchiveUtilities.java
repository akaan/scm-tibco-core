package scm.tibco.plugins;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;

public class ArchiveUtilities {

	public ArchiveUtilities() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 * @param srcJarFile
	 * @param file
	 * @param destName
	 * @return
	 * @throws IOException
	 */
	public static File updateJarFile(File srcJarFile, File file, String destName) throws IOException {
		File tmpJarFile = File.createTempFile("tempJar", ".tmp");
		JarFile jarFile = new JarFile(srcJarFile);
		boolean jarUpdated = false;

		try {
			JarOutputStream tempJarOutputStream = new JarOutputStream(
					new FileOutputStream(tmpJarFile));

			try {
				// Added the new files to the jar.

				FileInputStream fis = new FileInputStream(file);
				// adding the CDD as destName (default.cdd) to the jar.
				try {
					byte[] buffer = new byte[1024];
					int bytesRead = 0;
					JarEntry entry = new JarEntry(destName);
					tempJarOutputStream.putNextEntry(entry);
					while ((bytesRead = fis.read(buffer)) != -1) {
						tempJarOutputStream.write(buffer, 0, bytesRead);
					}

					System.out.println(entry.getName() + " added to "
							+ srcJarFile + ".");
				} finally {
					fis.close();
				}

				// Copy original jar file to the temporary one.
				Enumeration<JarEntry> jarEntries = jarFile.entries();
				while (jarEntries.hasMoreElements()) {
					JarEntry entry = (JarEntry) jarEntries.nextElement();
					InputStream entryInputStream = jarFile
							.getInputStream(entry);
					tempJarOutputStream.putNextEntry(entry);
					byte[] buffer = new byte[1024];
					int bytesRead = 0;
					while ((bytesRead = entryInputStream.read(buffer)) != -1) {
						tempJarOutputStream.write(buffer, 0, bytesRead);
					}
				}

				jarUpdated = true;
			} catch (Exception ex) {
				ex.printStackTrace();
				tempJarOutputStream.putNextEntry(new JarEntry("stub"));
			} finally {
				tempJarOutputStream.close();
			}

		} finally {
			jarFile.close();
			System.out.println(srcJarFile.getAbsolutePath() + " closed.");

			if (!jarUpdated) {
				tmpJarFile.delete();
			}
		}

		if (jarUpdated) {
			srcJarFile.delete();
			tmpJarFile.renameTo(srcJarFile);
			System.out.println(srcJarFile.getAbsolutePath() + " updated.");
		}

		return srcJarFile;
	}
	
	/**
	 * @param srcJarFile
	 * @param targetPackage
	 * @param filesToAdd
	 * @throws IOException
	 */
	public static File createJarFile(File srcJarFile, List<File> filesToAdd)
			throws IOException {
		boolean jarUpdated = false;

		try {
			JarOutputStream tempJarOutputStream = new JarOutputStream(
					new FileOutputStream(srcJarFile));

			try {
				// Added the new files to the jar.
				for (int i = 0; i < filesToAdd.size(); i++) {
					File file = (File) filesToAdd.get(i);
					FileInputStream fis = new FileInputStream(file);
					try {
						byte[] buffer = new byte[1024];
						int bytesRead = 0;
						JarEntry entry = new JarEntry(file.getName());
						tempJarOutputStream.putNextEntry(entry);
						while ((bytesRead = fis.read(buffer)) != -1) {
							tempJarOutputStream.write(buffer, 0, bytesRead);
						}

						System.out.println(entry.getName() + " added to "
								+ srcJarFile + ".");
					} finally {
						fis.close();
					}
				}
			} catch (Exception ex) {
				ex.printStackTrace();
				tempJarOutputStream.putNextEntry(new JarEntry("stub"));
			} finally {
				tempJarOutputStream.close();
			}

		} finally {
			System.out.println(srcJarFile.getAbsolutePath() + " closed.");

		}
		System.out.println(srcJarFile.getAbsolutePath() + " created.");
		return srcJarFile;
	}
	
	/**
	 * 
	 * @param arg0
	 * @param extractDirectory
	 * @return
	 * @throws IOException
	 */
	public static File extractpackage(File arg0, File extractDirectory)
			throws IOException {
		JarFile jar = new JarFile(arg0);
		Enumeration<JarEntry> enumEntries = jar.entries();
		File barFile = null;
		while (enumEntries.hasMoreElements()) {
			java.util.jar.JarEntry file = (java.util.jar.JarEntry) enumEntries
					.nextElement();
			java.io.File f = new java.io.File(extractDirectory, file.getName());
			System.out
					.println(file + " file [dir=" + file.isDirectory() + "].");

			if (file.isDirectory()) { // if its a directory, create it
				f.mkdir();
				continue;
			}
			if (file.getName().endsWith(".sar")) {
				barFile = f;
			}
			java.io.InputStream is = jar.getInputStream(file); // get the input
																// stream
			java.io.FileOutputStream fos = new java.io.FileOutputStream(f);
			while (is.available() > 0) { // write contents of 'is' to 'fos'
				fos.write(is.read());
			}
			fos.close();
			is.close();
			System.out.println(file + " extracted.");
		}
		return barFile;
	}

	/**
	 * 
	 * @param arg0
	 * @param extractDirectory
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static File repackage(File target, File extractDirectory)
			throws FileNotFoundException, IOException {

		JarOutputStream tempJarOutputStream = new JarOutputStream(
				new FileOutputStream(target));
		try {
			File[] filesToAdd = extractDirectory.listFiles();
			for (int i = 0; i < filesToAdd.length; i++) {
				File file = filesToAdd[i];
				FileInputStream fis = new FileInputStream(file);
				try {
					byte[] buffer = new byte[1024];
					int bytesRead = 0;
					JarEntry entry = new JarEntry(file.getName());
					tempJarOutputStream.putNextEntry(entry);
					while ((bytesRead = fis.read(buffer)) != -1) {
						tempJarOutputStream.write(buffer, 0, bytesRead);
					}

					System.out.println(entry.getName() + " added.");
				} finally {
					fis.close();
				}
			}
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
			tempJarOutputStream.putNextEntry(new JarEntry("stub"));
		} finally {
			tempJarOutputStream.close();
		}
		return target;
	}	

}
