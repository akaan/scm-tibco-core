package scm.tibco.plugins;

public class AppManageExitCodes {

	private String[] exitCode = new String[256];
	static AppManageExitCodes instance = null;

	public static AppManageExitCodes getInstance() {
		if (instance == null) {
			instance = new AppManageExitCodes();
		}
		return instance;
	}
	
	private AppManageExitCodes() {
		exitCode[0] = "AM_Succeeded"; // 0
		exitCode[255] = "AM_Usage_Error                 "; // -1
		exitCode[254] = "AM_Application_Does_Not_Exist  "; // -2
		exitCode[253] = "AM_Unexpected_Exception        "; // -3
		exitCode[252] = "AM_Unexpected_Throwable        "; // -4
		exitCode[251] = "AM_Service_Does_Not_Exist      "; // -5
		exitCode[250] = "AM_Binding_Does_Not_Exist      "; // -6
		exitCode[249] = "AM_Hawk_Microagent             "; // -7
		exitCode[248] = "AM_Hawk_Console                "; // -8
		exitCode[247] = "AM_No_Machine_Assoc_W_Process  "; // -9
		exitCode[246] = "AM_Pasring_XSD                 "; // -10
		exitCode[245] = "AM_Pasring_XML                 "; // -11
		exitCode[244] = "AM_Validation                  "; // -12
		exitCode[243] = "AM_Not_Authorized              "; // -13
		exitCode[242] = "AM_Not_Authenticated           "; // -14
		exitCode[241] = "AM_Domain_Not_Installed        "; // -15
		exitCode[240] = "AM_Domain_Master_Server_Down   "; // -16
		exitCode[239] = "AM_Get_Archive                 "; // -17
		exitCode[238] = "AM_Batch                       "; // -18
		exitCode[236] = "AM_Upload_General              "; // -20
		exitCode[235] = "AM_Upload_Commit               "; // -21
		exitCode[234] = "AM_Upload_Application_Archive  "; // -22
		exitCode[232] = "AM_Error_Executing_EAR_Plugin  "; // -24
		exitCode[226] = "AM_Config_Error_General        "; // -30
		exitCode[225] = "AM_Config_Commit               "; // -31
		exitCode[224] = "AM_Config_File_Parse_Error     "; // -32
		exitCode[223] = "AM_Config_File_Read_Error      "; // -33
		exitCode[222] = "AM_Config_File_Does_Not_Exist  "; // -34
		exitCode[221] = "AM_No_Uploaded_Archive         "; // -35
		exitCode[220] = "AM_Invalid_Repo_Instance       "; // -36
		exitCode[216] = "AM_Deploy_General              "; // -40
		exitCode[215] = "AM_Deploy_Commit               "; // -41
		exitCode[214] = "AM_Not_Deployable_State        "; // -42
		exitCode[213] = "AM_Deployment_Status           "; // -43
		exitCode[206] = "AM_Undeploy_General            "; // -50
		exitCode[205] = "AM_Undeploy_Commit             "; // -51
		exitCode[196] = "AM_Delete_General              "; // -60
		exitCode[195] = "AM_Delete_Commit               "; // -61
		exitCode[194] = "AM_Deployed_State              "; // -62
		exitCode[186] = "AM_Export_General              "; // -70
		exitCode[185] = "AM_XML_Serialize               "; // -71
		exitCode[184] = "AM_Export_File_Name_Error      "; // -72
		exitCode[183] = "AM_Merge_Error                 "; // -73
		exitCode[176] = "AM_Start_General               "; // -80
		exitCode[166] = "AM_Stop_General                "; // -90
		exitCode[166] = "AM_Stop_Not_In_Stoppable_State "; // -90
		exitCode[156] = "AM_Kill_General                "; // -100
	}

	public String toMessage(int value) {
		if (this.exitCode[value] != null) {
			return this.exitCode[value];
		}
		return "unknown exit code : " + value;
	}

}
