/**
 * 
 */
package scm.tibco.plugins;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.util.Iterator;
import java.util.Properties;

import javax.xml.bind.JAXBElement;

import org.apache.maven.shared.filtering.MavenFileFilterRequest;
import org.codehaus.plexus.interpolation.InterpolationException;
import org.codehaus.plexus.interpolation.PropertiesBasedValueSource;
import org.codehaus.plexus.interpolation.ValueSource;
import org.codehaus.plexus.interpolation.multi.MultiDelimiterStringSearchInterpolator;

import scm.core.ScmWorkingRepository;

import com.fedex.scm.tibco.project.DeploymentPlan;
import com.fedex.scm.tibco.project.DeploymentPlan.Nodes;
import com.fedex.scm.tibco.project.GlobalDefinitionSet;
import com.fedex.scm.tibco.project.GlobalVariablesDef;
import com.fedex.scm.tibco.project.Node;
import com.tibco.xmlns.applicationmanagement.ApplicationType;
import com.tibco.xmlns.applicationmanagement.Binding;
import com.tibco.xmlns.applicationmanagement.ObjectFactory;
import com.tibco.xmlns.applicationmanagement.RepoType;
import com.tibco.xmlns.applicationmanagement.ServiceType;

/**
 * @author akaan
 * 
 */
public class TransformUtils {

	/**
	 * Applying the global definition and merge it with the current plan.
	 * 
	 * @param globalId
	 * 
	 * @param aNode
	 * @param properties
	 * @return
	 */
	public static Node applyGlobalDefinitions(Nodes nodes, Node aNode,
			Properties properties) {
		if (nodes == null && aNode == null) {
			return null;
		}
		if (nodes != null && nodes.getGlobalId() == null
				&& aNode.getGlobalId() == null) {
			throw new NullPointerException(
					"The GlobalId of the Nodes and Node is null.");
		}
		if (nodes == null && aNode.getGlobalId() == null) {
			throw new NullPointerException("The GlobalId of the Node is null.");
		}
		final ValueSource propertiesValueSource = new PropertiesBasedValueSource(
				properties);
		MavenFileFilterRequest request = new MavenFileFilterRequest();
		MultiDelimiterStringSearchInterpolator interpolator = new MultiDelimiterStringSearchInterpolator();
		interpolator.setDelimiterSpecs(request.getDelimiters());
		interpolator.addValueSource(propertiesValueSource);

		if (aNode.isSetNodeNumber()) {
			properties.setProperty("nodeNumber",
					String.format("%02d", aNode.getNodeNumber()));
		}

		GlobalVariablesDef definition = aNode.getGlobalId() != null ? (GlobalVariablesDef) aNode
				.getGlobalId() : (GlobalVariablesDef) nodes.getGlobalId();

		if (aNode.isSetEngineBaseName()) {
			if (!aNode.isSetEngineName()) {
				try {
					aNode.setEngineName(interpolator.interpolate(aNode
							.getEngineBaseName()));
				} catch (InterpolationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				aNode.setEngineName(aNode.getEngineBaseName());
			}
		} else {
			if (definition.isSetEngineBaseName()) {
				try {
					if (!aNode.isSetEngineName()) {
						aNode.setEngineName(interpolator.interpolate(definition
								.getEngineBaseName()));
					}
					aNode.setEngineBaseName(definition.getEngineBaseName());
				} catch (InterpolationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				// means there is no engine name defined and no basenames to be
				// found.
				throw new NullPointerException(
						"Both definition and node do not have engine references");
			}
		}

		if (aNode.isSetMachineBaseName()) {
			if (!aNode.isSetMachineName()) {
				try {
					aNode.setMachineName(interpolator.interpolate(aNode
							.getMachineBaseName()));
				} catch (InterpolationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				// do nothing keep the machine name
			}
		} else {
			if (definition.isSetMachineBaseName()) {
				try {
					if (!aNode.isSetMachineName()) {
						aNode.setMachineName(interpolator
								.interpolate(definition.getMachineBaseName()));
					}
					aNode.setMachineBaseName(definition.getMachineBaseName());
				} catch (InterpolationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		if (!aNode.isSetActivationLimit()) {
			aNode.setActivationLimit(definition.isActivationLimit());
		}
		if (!aNode.isSetAppendClasspath()) {
			try {
				aNode.setAppendClasspath(interpolator.interpolate(definition
						.getAppendClasspath()));
			} catch (InterpolationException e) {
				e.printStackTrace();
			}
		}
		if (!aNode.isSetInitialMemoryJVM()) {
			aNode.setInitialMemoryJVM(definition.getInitialMemoryJVM());
		}
		if (!aNode.isSetMaximumMemoryJVM()) {
			aNode.setMaximumMemoryJVM(definition.getMaximumMemoryJVM());
		}
		if (!aNode.isSetMaxLogFileCount()) {
			aNode.setMaxLogFileCount(definition.getMaxLogFileCount());
		}
		if (!aNode.isSetMaxLogFileSize()) {
			aNode.setMaxLogFileSize(definition.getMaxLogFileSize());
		}
		if (!aNode.isSetPrependClasspath()) {
			aNode.setPrependClasspath(definition.getPrependClasspath());
		}
		if (!aNode.isSetThreadCount()) {
			aNode.setThreadCount(definition.getThreadCount());
		}
		if (!aNode.isSetThreadStackSize()) {
			aNode.setThreadStackSize(definition.getThreadStackSize());
		}
		if (!aNode.isSetTypeOfFault()) {
			aNode.setTypeOfFault(definition.isTypeOfFault());
		}
		return aNode;
	}

	public static void applyGlobalDefinitions(Nodes nodes, Properties p) {
		assert nodes != null;
		for (Iterator<Node> iterator = nodes.getNode().iterator(); iterator
				.hasNext();) {
			Node type = iterator.next();
			p.setProperty("nodeNumber",
					String.format("%02d", type.getNodeNumber()));
			applyGlobalDefinitions(nodes, type, p);
		}
	}

	/**
	 * 
	 * @param plan
	 * @param node
	 * @return
	 */
	public static String computeEngineName(DeploymentPlan plan, Node node) {
		GlobalVariablesDef definition = node.getGlobalId() != null ? (GlobalVariablesDef) node
				.getGlobalId() : (GlobalVariablesDef) plan.getNodes()
				.getGlobalId();

		return interpolateEngineName(node.getEngineName(),
				node.getEngineBaseName(), definition, node.getNodeNumber(),
				plan.getNumberingStrategy());
	}

	/**
	 * 
	 * @param plan
	 * @param node
	 * @return
	 */
	public static String computeMachineName(DeploymentPlan plan, Node node) {
		GlobalVariablesDef definition = node.getGlobalId() != null ? (GlobalVariablesDef) node
				.getGlobalId() : (GlobalVariablesDef) plan.getNodes()
				.getGlobalId();

		return interpolateMachineName(node.getMachineName(),
				node.getMachineBaseName(), definition, node.getNodeNumber(),
				plan.getNumberingStrategy());
	}

	/**
	 * @param destinationRef
	 * @param upstream
	 * @param suffix
	 * @return
	 * @throws IOException
	 */
	public static File createRemoteWorkDirectoryAndTempFile(
			ScmWorkingRepository remote, boolean upstream, String suffix)
			throws IOException {
		assert remote != null;
		assert suffix != null;
		File f = File.createTempFile(upstream ? "upstream-" : "downstream-",
				suffix.startsWith(".") ? suffix : "." + suffix);
		return createRemoteWorkFileRef(remote, f);
	}

	/**
	 * 
	 * @param destinationRef
	 * @param upstream
	 * @return
	 * @throws IOException
	 */
	public static File createRemoteWorkDirectoryAndTempTextFile(
			ScmWorkingRepository destinationRef, boolean upstream)
			throws IOException {
		assert destinationRef != null;
		File f = File.createTempFile(upstream ? "upstream-" : "downstream-",
				".txt");
		return createRemoteWorkFileRef(destinationRef, f);
	}

	/**
	 * @param remote
	 * @param upstream
	 * @return
	 * @throws IOException
	 */
	public static File createRemoteWorkDirectoryAndTempXmlFile(
			ScmWorkingRepository remote, boolean upstream) throws IOException {
		assert remote != null;
		File f = File.createTempFile(upstream ? "upstream-" : "downstream-",
				".xml");
		return createRemoteWorkFileRef(remote, f);
	}

	/**
	 * @param hostAndDirectory
	 * @param name
	 * @return
	 * @throws IOException
	 */
	/**
	 * @param hostAndDirectory
	 * @param name
	 * @return
	 * @throws IOException
	 */
	public static File createRemoteWorkFileRef(ScmWorkingRepository remote,
			File name) throws IOException {
		assert name != null;
		assert remote != null;
		return new File(remote.getBasedir(), name.getName());
	}

	/**
	 * 
	 * @param deploymentPlan
	 * @return
	 * @throws IntrospectionException
	 */
	public static Properties extractGlobals(DeploymentPlan deploymentPlan)
			throws IntrospectionException {
		Properties p = new Properties();
		BeanInfo info = Introspector.getBeanInfo(deploymentPlan.getClass());
		for (PropertyDescriptor pd : info.getPropertyDescriptors()) {
			Method m = pd.getReadMethod();
			Object _value = null;
			try {
				_value = m.invoke(deploymentPlan, (Object[]) null);
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (_value != null
					&& !String.valueOf(_value.getClass()).startsWith(
							"com.fedex.scm.tibco.project")) {
				p.put(pd.getName(), String.valueOf(_value));
			}
		}
		return p;
	}

	protected static String interpolateEngineName(String engineName,
			String engineBaseName, GlobalVariablesDef nodeId,
			Object nodeNumber, int numberingStrategy) {

		// System.out.printf("%s %s \n\t%s %s \n", engineName, engineBaseName,
		// nodeId.getId(), nodeNumber);
		if (nodeNumber != null) {
			nodeNumber = new Integer(String.valueOf(nodeNumber)).intValue()
					+ numberingStrategy;
		} else {
			nodeNumber = 0;
		}
		if (engineBaseName != null && engineBaseName.equals(engineName)) {
			String s = String.format(engineBaseName, nodeNumber);
			return s;
		}
		// no interpolation required.
		if (engineName != null) {
			if (engineName.indexOf("%") > -1) {
				return String.format(engineName, nodeNumber);
			} else {
				return engineName;
			}
		}
		if (engineBaseName != null) {
			if (engineBaseName.indexOf("%") > -1) {
				return String.format(engineBaseName, nodeNumber);
			} else {
				return engineBaseName;
			}
		} else {
			if (nodeId != null) {
				GlobalDefinitionSet set = (GlobalDefinitionSet) nodeId;
				if (set.getEngineBaseName() != null) {
					return interpolateEngineName(null, set.getEngineBaseName(),
							null, nodeNumber, numberingStrategy);
				} else {
					throw new RuntimeException("Missing Engine name");
				}
			}
		}
		return "";
	}

	protected static String interpolateMachineName(String machineName,
			String machineBaseName, GlobalVariablesDef nodeId,
			Object nodeNumber, int numberingStrategy) {
		// no interpolation required.
		// System.err.printf("-->\t%s \n\t%s\n\t%s\n\t%s \n",machineName,
		// machineBaseName, nodeId.getId(), nodeNumber);
		// if the basename is the same as the name
		// nodeNumber = new Integer(((Integer) nodeNumber).intValue() +
		// numberingStrategy);
		if (machineBaseName != null && machineBaseName.equals(machineName)) {
			String s = String.format(machineBaseName, nodeNumber);
			return s;
		}
		if (machineName != null) {
			if (machineName.indexOf("%") > -1) {
				return String.format(machineName, nodeNumber);
			} else
				return machineName;
		}
		if (machineBaseName != null) {
			if (machineBaseName.indexOf("%") > -1) {
				return String.format(machineBaseName, nodeNumber);
			} else
				return machineBaseName;
		} else {
			if (nodeId != null) {
				GlobalDefinitionSet set = (GlobalDefinitionSet) nodeId;
				if (set.getMachineBaseName() != null) {
					return interpolateMachineName(null,
							set.getMachineBaseName(), null, nodeNumber,
							numberingStrategy);
				} else {
					throw new RuntimeException(
							"Missing Machine name reference or base name");
				}
			}
		}
		return "";
	}

	/**
	 * 
	 * @param type
	 */
	public static void interpolateProjectWithApplication(
			DeploymentPlan finalPlan, ApplicationType type,
			Properties properties) {
		// set
		// or die "Unable to open deployment xml file , $$output_file";
		// foreach $d_var (@dep_var) {
		// if ( index( $d_var, "application xmlns" ) gt 0 ) {
		// print OUTPUT
		// "<application xmlns=\"http://www.tibco.com/xmlns/ApplicationManagement\" name=\"$project\">\n";
		type.setName(finalPlan.getProjectName());
		Iterator<JAXBElement<? extends ServiceType>> iter = type.getServices()
				.getBaseService().iterator();
		while (iter.hasNext()) {
			JAXBElement<? extends ServiceType> l = iter.next();
			ServiceType serviceType = l.getValue();

			Nodes entry = finalPlan.getNodes();

			serviceType.setName(TransformUtils.getServiceName(finalPlan));
			serviceType.getBindings().getBinding().clear();

			for (Iterator<Node> iterator = entry.getNode().iterator(); iterator
					.hasNext();) {
				Node node = iterator.next();
				Binding binder = TransformUtils.nodeToBinding(finalPlan, node,
						properties);
				serviceType.getBindings().getBinding().add(binder);
			}
		}
		type.getRepoInstances().getRvRepoInstance()
				.setDaemon(finalPlan.getDaemon());
		if (finalPlan.getVersion() > 5.3) {
			type.getRepoInstances().setSelected(RepoType.LOCAL);
		}

	}

	/**
	 * 
	 * @param finalPlan
	 * @return
	 */
	protected static String getServiceName(DeploymentPlan finalPlan) {
		assert finalPlan != null;
		String extension = "par";
		if (finalPlan.getType() != null) {
			if ("be-engine".equals(finalPlan.getType())) {
				extension = "bar";
			}
		}
		return String.format("%s.%s", finalPlan.getComponentName(), extension);
	}

	/**
	 * 
	 * @param plan
	 * @param node
	 * @return
	 */
	public static Binding nodeToBinding(DeploymentPlan plan, Node node,
			Properties properties) {
		ObjectFactory factory = new ObjectFactory();
		Binding newBinding = new Binding();

		// make sure that the node number is filled out otherwise null pointer
		// exception
		if (!node.isSetNodeNumber()) {

			if (properties.containsKey("nodeNumber")) {
				node.setNodeNumber(Integer.valueOf(
						properties.getProperty("nodeNumber")).intValue());
			} else {
				// lame but we need to have a unique number
				node.setNodeNumber(Long.valueOf(System.currentTimeMillis())
						.intValue());
			}
		}

		newBinding.setName(computeEngineName(plan, node));
		newBinding.setMachine(computeMachineName(plan, node));

		newBinding.setProduct(factory.createProduct());
		String type = plan.getType();
		if (type == null) {
			type = "BW";
		}
		
		newBinding.getProduct().setType(type);
		newBinding.getProduct().setVersion(String.valueOf(plan.getVersion()));
		newBinding.getProduct().setLocation(plan.getToolHome());
		newBinding.setSetting(factory.createSetting());
		newBinding.getSetting().setJava(factory.createSettingJava());
		newBinding.getSetting().getJava()
				.setPrepandClassPath(node.getPrependClasspath());
		newBinding.getSetting().getJava()
				.setAppendClassPath(node.getAppendClasspath());
		if (node.getInitialMemoryJVM() != null) {
			newBinding.getSetting().getJava()
					.setInitHeapSize(toBigInteger(node.getInitialMemoryJVM()));
		}
		if (node.getMaximumMemoryJVM() != null)
			newBinding.getSetting().getJava()
					.setMaxHeapSize(toBigInteger(node.getMaximumMemoryJVM()));
		if (node.getThreadStackSize() != null)
			newBinding
					.getSetting()
					.getJava()
					.setThreadStackSize(toBigInteger(node.getThreadStackSize()));
		if (node.getMaxLogFileCount() != null)
			newBinding.getSetting().setMaxLogFileCount(
					toBigInteger(node.getMaxLogFileCount()));
		if (node.getMaxLogFileSize() != null)
			newBinding.getSetting().setMaxLogFileSize(
					toBigInteger(node.getMaxLogFileSize()));
		if (node.getThreadCount() != null)
			newBinding.getSetting().setThreadCount(
					toBigInteger(node.getThreadCount()));
		return newBinding;
	}

	/**
	 * 
	 * @param plan
	 * @return
	 */
	public static DeploymentPlan resizeNodes(DeploymentPlan plan) {
		Nodes nodes = plan.getNodes();
		int lastNodeNumber = 0;
		int max = Math.max(nodes.getNode().size(), plan.getNumberOfNodes());
		for (int i = 0; i < max; i++) {
			if (i >= plan.getNumberOfNodes()) {
				nodes.getNode().remove(nodes.getNode().size() - 1);
				continue;
			}
			if (i >= nodes.getNode().size()) {
				Node n = new Node(); // .cloneNode(lastNode);
				n.setGlobalId(plan.getNodes().getGlobalId());
				n.setNodeNumber(++lastNodeNumber);
				nodes.getNode().add(n);
			} else {
				// System.err.println(nodes.getNode().get(i));
				Node node = nodes.getNode().get(i);
				if (node.isSetNodeNumber()) {
					lastNodeNumber = node.getNodeNumber();
				} else {
					++lastNodeNumber;
					node.setNodeNumber(lastNodeNumber);
				}
			}
		}
		return plan;
	}

	public static BigInteger toBigInteger(Integer value) {
		BigInteger bg = new BigInteger(String.valueOf(value));
		return bg;
	}

}
