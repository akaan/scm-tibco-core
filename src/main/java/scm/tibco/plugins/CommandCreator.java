package scm.tibco.plugins;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class CommandCreator {

	public static boolean WINDOWS_FLAG = false;
	private static final Pattern WINDOWS_PATH = Pattern.compile("\\w:(\\S+)");

	/**
	 * 
	 * @param home
	 * @param version
	 * @return
	 */
	public static String getPath(File home) {
		return String.format("%s", toOSType(home.getAbsolutePath()));
	}

	// my @args = (
	// "$bin_dir/AppManage -config -deployconfig $file_in -app $project -ear $ear -domain $domain -user $user -pw $password 1>> $AppManage_log 2>&1"
	// );

	/**
	 * 
	 * @param home
	 * @param version
	 * @return
	 */
	public static String getPath(File home, String version) {
		if (home == null) {
			throw new NullPointerException("The tempFile parameter is not set.");
		}
		if (version == null) {
			version = "0.0.1";
		}
		return getPath(home.getAbsolutePath(), version);
	}

	// ${APPMANAGE} -deploy -ear "${EAR_FILE}" -deployconfig
	// "${DEPLOY_XML_FILE}" -app ${APPLICATION_NAME} -domain ${DOMAIN} -user
	// "${USER_NAME}" -pw "${PASSWORD}" ${FORCE_FLAG} ${START_ON_DEPLOY}

	/**
	 * 
	 * @param home
	 * @param version
	 * @return
	 */
	public static String getPath(String home, String version) {
		if (home == null) {
			throw new NullPointerException("The tempFile parameter is not set.");
		}
		if (version == null) {
			version = "0.0.1";
		}
		String path = String.format("%s/tra/%s/bin", toOSType(home), version);
		return path;
	}

	/**
	 * 
	 * @param home
	 * @param version
	 * @return
	 */
	public static String getStudioPath(String home, String version) {
		if (home == null) {
			throw new NullPointerException("The tempFile parameter is not set.");
		}
		if (version == null) {
			version = "0.0.1";
		}
		String path = String.format("%s/be/%s/studio/bin", toOSType(home), version);
		return path;
	}

	/**
	 * 
	 * @param home
	 * @param version
	 * @return
	 */
	public static String getStudioPath(File home, String version) {
		if (home == null) {
			throw new NullPointerException("The tempFile parameter is not set.");
		}
		if (version == null) {
			version = "0.0.1";
		}
		return getStudioPath(home.getAbsolutePath(), version);
	}

	public static boolean isLinux() {
		return System.getProperty("os.name").equals("Linux");
	}

	public static boolean isWindows() {
		if (WINDOWS_FLAG) {
			return true;
		}
		return System.getProperty("os.name").startsWith("Windows");
	}

	// my @args = (
	// "$bin_dir/AppManage -config -deployconfig $file_in -app $project -ear $ear -domain $domain -user $user -pw $password 1>> $AppManage_log 2>&1"
	// );

	public static Object toOSType(String absolutePath) {
		if (isWindows()) {
			absolutePath = absolutePath.replace("\\", "/");

			Matcher m = WINDOWS_PATH.matcher(absolutePath);
			if (m.matches()) {
				absolutePath = m.group(1);
			}
		}
		return absolutePath;
	}

	/**
	 * createAppManageDeployApplication -
	 * 
	 * @param tibcoHome
	 * @param tibcoVersion
	 * @param appName
	 * @param domainName
	 * @param userName
	 * @param password
	 * @param tempFile
	 * @param earFile
	 * @param forceFlag
	 * @param serializeFlag
	 * @param nostartFlag
	 * @return
	 */
	public static String createAppManageDeployApplication(boolean background,
			File tibcoHome, String tibcoVersion, String appName,
			String domainName, String userName, String password, File tempFile,
			File earFile, boolean forceFlag, boolean serializeFlag,
			boolean nostartFlag) {
		if (tempFile == null) {
			throw new NullPointerException("The tempFile parameter is not set.");
		}
		if (earFile == null) {
			throw new NullPointerException("The earFile parameter is not set.");
		}
		if (tibcoHome == null) {
			throw new NullPointerException(
					"The tibcoHome parameter is not set.");
		}
		/* "$bin_dir/AppManage -config -deployconfig $file_in -app $project -ear $ear -domain $domain -user $user -pw $password 1>> $AppManage_log 2>&1" */
		String deployCommand = String
				.format("./AppManage -deploy -deployconfig %s -app %s -ear %s -domain %s -user %s -pw %s %s %s",
						CommandCreator.toOSType(tempFile.getAbsolutePath()),
						appName, CommandCreator.toOSType(earFile
								.getAbsolutePath()), domainName, userName,
						password, forceFlag ? "-force" : "",
						serializeFlag ? "-serialize" : "",
						nostartFlag ? "-nostart" : "");
		if (background) {
			String logName = appName.replaceAll("\\W", "-");
			deployCommand = String.format(
					"sh -c \"( ( nohup %s 2>&1 1>> /tmp/%s.log ) & )\"",
					deployCommand, logName);
		}
		String command = String.format("cd %s && %s",
				CommandCreator.getPath(tibcoHome, tibcoVersion), deployCommand);
		return command;
	}

	/**
	 * my @args = (
	 * "$bin_dir_in/AppManage -export -out $temp_file -ear $ear_in 1>> $AppManage_log 2>&1"
	 * );
	 * 
	 * @param tibcoHome
	 * @param tibcoVersion
	 * 
	 * @return
	 */
	public static String createAppManageExportApplication(File tibcoHome,
			String tibcoVersion, File tempFile, File earFile) {
		if (tempFile == null) {
			throw new NullPointerException("The tempFile parameter is not set.");
		}
		if (earFile == null) {
			throw new NullPointerException("The earFile parameter is not set.");
		}
		if (tibcoHome == null) {
			throw new NullPointerException(
					"The tibcoHome parameter is not set.");
		}
		if (tibcoVersion == null) {
			tibcoVersion = "0.0.1";
		}
		String command = "";
		command = String.format(
				"cd %s && ./AppManage -export -out %s -ear %s ",
				getPath(tibcoHome, tibcoVersion),
				toOSType(tempFile.getAbsolutePath()),
				toOSType(earFile.getPath()));
		return command;
	}

	/**
	 * <pre>
	 * No user name or password specified
	 * 
	 * Usage:  AppManage -undeploy -app <app> -domain <domain> -user <user> -pw <password> [-cred <cred>] [-timeout <timeout>]
	 *         (to undeploy an application)
	 * 
	 * where args include:
	 * <app>          name of an application (case sensitive)
	 *                May contain folder if the application is not top level
	 *                Folder name should be separated by slash "/"
	 * <domain>       name of a domain (case sensitive)
	 * <user>         name of an authorized user that has read/write permission of the application(s)
	 *                and read/write permission to access data of SYS_<domain>
	 * <password>     password of an authorized user (case sensitive)
	 *                Should not be encrypted
	 * <cred>         name of a property file containing user and encrypted password
	 *                Use obfuscate.exe to encrypt the property file
	 *                If specified, no -user and -pw should be specified
	 * <timeout>      If specified, kill the service within <timeout> seconds
	 * 
	 * Example:
	 * </pre>
	 * 
	 * @param tibcoHome
	 * @param tibcoVersion
	 * @param projectName
	 * @param domainName
	 * @param adminUsername
	 * @param adminPassword
	 * @return
	 */
	public static String createAppManageUndeploy(File tibcoHome,
			String tibcoVersion, String projectName, String domainName,
			String adminUsername, String adminPassword) {
		String command = String
				.format("cd %s && ./AppManage -undeploy -app %s -domain %s -user %s -pw %s ",
						getPath(tibcoHome, tibcoVersion), projectName,
						domainName, adminUsername, adminPassword);
		return command;
	}

	/**
	 * 
	 * @param tibcoHome
	 * @param tibcoVersion
	 * @param logFileName
	 * @param remoteFileRef
	 * @return
	 */
	public static String createAppManageTailLog(File tibcoHome,
			String tibcoVersion, String logFileName, File remoteFileRef) {
		if (logFileName == null) {
			throw new NullPointerException(
					"The logFileName parameter is not set.");
		}
		if (tibcoHome == null) {
			throw new NullPointerException(
					"The tibcoHome parameter is not set.");
		}
		String command = String.format(
				"cd %s && [ -f ../%s ] && tail -40 ../%s > %s",
				getPath(tibcoHome, tibcoVersion), logFileName, logFileName,
				remoteFileRef.getAbsolutePath());
		return command;
	}
	
	
	/**
	 * 
	 * <pre>
	 * [tibco@drh00280 bin]$ ./AppManage -delete -h
	 * 
	 * No user name or password specified
	 * 
	 * Usage:  AppManage -delete -app <app> -domain <domain> [-user <user> -pw <password>] [-cred <cred>] [-force] [-timeout <timeout>]
	 *         (to delete an application
	 *          and optionally undeploy an application if -force is specified)
	 * 
	 * where args include:
	 * <app>          name of an application (case sensitive)
	 *                May contain folder if the application is not top level
	 *                Folder name should be separated by slash "/"
	 * <domain>       name of a domain (case sensitive)
	 * <user>         name of an authorized user that has read/write permission of the application(s)
	 *                and read/write permission to access data of SYS_<domain>
	 * <password>     password of an authorized user (case sensitive)
	 *                Should not be encrypted
	 * <cred>         name of a property file containing user and encrypted password
	 *                Use obfuscate.exe to encrypt the property file
	 *                If specified, no -user and -pw should be specified
	 * -force         If specified, undeploy the application first, then delete
	 *                If not specified, an application can not be deleted
	 *                unless it is first undeployed
	 * <timeout>      If specified, kill the service within <timeout> seconds
	 *                Only Apply when -force is used
	 * 
	 * Example:
	 * AppManage -delete -app myApp -user a -pw a -domain test -force
	 * [tibco@drh00280 bin]$
	 * </pre>
	 * 
	 * @param mojo
	 * @param appName
	 * @param domainName
	 * @param adminUsername
	 * @param adminPassword
	 * @return
	 */
	public static String createAppManageDeleteProcess(File tibcoHome,
			String tibcoVersion, String appName, String domainName,
			String adminUsername, String adminPassword) {
		String command = String
				.format("cd %s && ./AppManage -delete -app %s -domain %s -user %s -pw %s -force",
						getPath(tibcoHome, tibcoVersion), appName, domainName,
						adminUsername, adminPassword);
		return command;
	}

}
