/**
 * 
 */
package scm.tibco.plugins;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.maven.plugins.annotations.Parameter;

/**
 * @author akaan
 *
 */
public class Target {

	public static class GlobalVariableSet {

		@Parameter
		public List<File> globalVariableRefs = new ArrayList<File>();
		@Parameter
		public Properties globalVariables;
	}

	public static class Resource {
		@Parameter
		public String source;
		@Parameter
		public String destName;
		@Parameter
		public File outputDirectory;
	}

	/**
	 * The id of the target
	 */
	@Parameter
	public String id;

	/**
	 * The associated plan id for this target in the plan.xml
	 */
	@Parameter(required = true)
	public String planId;

	/**
	 * /** The administration host and the directory where we transfer the
	 * archive (ear) and use as a working directory. The format is
	 * [hostname]:[directory] example: scpexe://home.prod.ux.com:/tmp/myapp
	 */
	@Parameter
	public String adminRepository;

	@Parameter
	public String finalName;

	@Parameter
	public GlobalVariableSet globalVariableSet;

	@Parameter
	public List<Resource> includes = new ArrayList<Resource>();

	/**
	 * 
	 */
	public Target() {
		// TODO Auto-generated constructor stub
	}
}
