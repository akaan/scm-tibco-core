/**
 * 
 */
package scm.tibco.plugins;

import org.apache.maven.wagon.Wagon;

import scm.core.RemoteExecutor;

/**
 * @author akaan
 *
 */
public class TibcoRemoteExecutor extends RemoteExecutor {

	final Wagon wagon;
	final AbstractTibcoGlobalMojo plugin;
	/**
	 * @param plugin
	 */
	public TibcoRemoteExecutor(AbstractTibcoGlobalMojo plugin) {
		super(plugin);
		this.plugin = plugin;
		this.wagon = plugin.getWagon();
	}

	/* (non-Javadoc)
	 * @see scm.core.RemoteExecutor#getWagon()
	 */
	@Override
	public Wagon getWagon() {
		// TODO Auto-generated method stub
		return wagon;
	}
	
	public AbstractTibcoGlobalMojo getPlugin() {
		return this.plugin;
	}
	

}
