package scm.tibco.plugins;

import java.beans.IntrospectionException;
import java.io.File;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

import javax.xml.transform.stream.StreamSource;

import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.shared.filtering.MavenFileFilterRequest;
import org.apache.maven.shared.filtering.MavenFilteringException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import scm.core.ScmWorkingRepository;

import com.fedex.scm.tibco.project.DeploymentPlan;
import com.fedex.scm.tibco.project.DeploymentPlan.Nodes;
import com.fedex.scm.tibco.project.Node;
import com.fedex.scm.tibco.project.Project;

/**
 * 
 * @author akaan
 * 
 */
@SuppressWarnings("deprecation")
public abstract class AbstractTibcoMojo extends AbstractTibcoGlobalMojo {

	private ApplicationContext CONTEXT;
	protected Jaxb2Marshaller marshaller;
	/**
	 * The specifc plan id to be executed. By default the first plan is executed
	 * if no other plan information is supplied.
	 */
	@Parameter(property = "remote.plugin.plan")
	protected String planId;
	
	
	protected Jaxb2Marshaller unmarshaller;

	/**
	 * 
	 */
	public AbstractTibcoMojo() {
		super();
		initialize();
		this.protocol = "scpexe";
	}




	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * scm.core.AbstractRemoteInvocationMojo#executeCommandExec(org.apache.maven
	 * .wagon.Wagon, java.lang.String)
	 */
	public void executeCommandExec( String command)
			throws MojoExecutionException {
		try {
			command = command.replace("\n", "");
			command = command.replace("\r", "");
			executeCommandExec(getWagon(), command);
		} catch (Exception e) {
			throw new MojoExecutionException(e.getMessage(), e);
		}
	}

	/**
	 * @return the marshaller
	 */
	public final Jaxb2Marshaller getMarshaller() {
		return marshaller;
	}



	/**
	 * @return the mavenSession
	 */
	public final MavenSession getMavenSession() {
		return session;
	}


	/**
	 * @return the unmarshaller
	 */
	public final Jaxb2Marshaller getUnmarshaller() {
		return unmarshaller;
	}

	/**
	 * 
	 * @return
	 */
	public ApplicationContext initialize() {
		CONTEXT = new ClassPathXmlApplicationContext("/applicationContext.xml",
				this.getClass());
		unmarshaller = (Jaxb2Marshaller) CONTEXT.getBean("jaxb2Unmarshaller");
		marshaller = (Jaxb2Marshaller) CONTEXT.getBean("jaxb2Marshaller");

		/* load plans */
		return null;
	}	
	
	
	/**
	 * 
	 * @param distributionFile
	 * @param _planId
	 * @return
	 * @throws MojoFailureException
	 * @throws MojoExecutionException
	 */
	@SuppressWarnings("unchecked")
	public DeploymentPlan initializePlans(File distributionFile, String _planId)
			throws MojoFailureException, MojoExecutionException {

		/*
		 * Initialize and load the plan file and extract the plan.
		 */

		if (this.unmarshaller == null) {
			throw new MojoFailureException(
					"The unmarshaller has not been instantiated.");
		}
		if (distributionFile == null) {
			throw new MojoExecutionException(
					"Please define the distributionFile!");
		} else if (!distributionFile.exists() || !distributionFile.isFile()) {
			throw new MojoExecutionException(
					"Please define the distributionFile '" + distributionFile
							+ "' as a file and make sure it exists.");
		}

		// filter the plan first before loading.
		// DeploymentFileFilter filter = new DeploymentFileFilter();

		File outputPlan = new File(this.outputDirectory,
				distributionFile.getName());
		MavenFileFilterRequest req = new MavenFileFilterRequest(
				distributionFile, outputPlan, true, project,
				this.project.getFilters(), true, "UTF-8", session, null);
		try {
			if (this.outputDirectory != null && !this.outputDirectory.exists()) {
				if (!this.outputDirectory.mkdirs()) {
					throw new MojoFailureException(
							"Failure to create directories to publish plan to "
									+ this.outputDirectory);
				}
			}
			if (this.stagingDirectory != null
					&& !this.stagingDirectory.exists()) {
				if (!this.stagingDirectory.mkdirs()) {
					throw new MojoFailureException(
							"Failure to create directories to publish plan to "
									+ this.stagingDirectory);
				}
			}
			this.mavenFileFilter.copyFile(req);
		} catch (MavenFilteringException e1) {
			// TODO Auto-generated catch block
			throw new MojoFailureException("Publishing plan", e1);
		}

		message("Initialized filtered plan from " + outputPlan);

		Project myproject = (Project) this.unmarshaller
				.unmarshal(new StreamSource(outputPlan));
		if (!myproject.isSetPlans() || !myproject.getPlans().isSetPlan()) {
			throw new MojoExecutionException("No plans defined for project '"
					+ myproject.getName() + "'.");
		}

		/*
		 * Getting the plan out of a list of plans.
		 */
		if (_planId == null) {
			Vector<String> v = new Vector<String>();
			for (DeploymentPlan plans : myproject.getPlans().getPlan()) {
				v.add(plans.getId());
			}
			throw new MojoExecutionException("Please define the planId, available ids are "
					+ Arrays.toString(v.toArray()) + "!");
		}
		DeploymentPlan plan = processPlans(myproject.getPlans().getPlan(), _planId);
		if (plan.isSetAdminRepository()) {
			try {
				this.adminRepositorySystem = new ScmWorkingRepository(this,
						plan.getAdminRepository());
			} catch (MalformedURLException e) {
				throw new MojoExecutionException(
						"Illegal repository format for "
								+ plan.getAdminRepository(), e);
			}
		}
		if (this.adminRepository == null) {
			throw new MojoExecutionException(
					"Please define the adminRepository either in the configuration or plan!");
		}
		message("Using " + this.adminRepository
				+ " as the administration repository.");

		return plan;
	}

		

	/**
	 * @param plans
	 * @param planId
	 */
	public DeploymentPlan processPlans(List<DeploymentPlan> plans, String planId) {
		DeploymentPlan plan = plans.get(0);
		for (Iterator<DeploymentPlan> iterator = plans.iterator(); iterator
				.hasNext();) {
			DeploymentPlan deploymentPlan = iterator.next();
			if (deploymentPlan.getId() != null
					&& deploymentPlan.getId().equals(planId)) {
				plan = deploymentPlan;
				break;
			}
		}
		// validate the project and
		Properties properties = null;
		try {
			properties = TransformUtils.extractGlobals(plan);
		} catch (IntrospectionException e) {
			e.printStackTrace();
		}
		Nodes nodes = plan.getNodes();

		getLog().info(
				"processing '" + nodes.getNode().size()
						+ "' nodes for the deployment '"
						+ plan.getProjectName() + "' with plan-id '"
						+ plan.getId() + "'.");
		for (Iterator<Node> nodeIterator = nodes.getNode().iterator(); nodeIterator
				.hasNext();) {
			Node node = nodeIterator.next();
			node = TransformUtils.applyGlobalDefinitions(nodes, node,
					properties);
			// getLog().debug(node.toString());
		}
		return plan;
	}

	/**
	 * @param planId
	 *            the planId to set
	 */
	public final void setPlanId(String planId) {
		this.planId = planId;
	}



}
